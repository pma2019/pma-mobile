package org.pma.fitnessmatcher;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.pma.fitnessmatcher.adapter.TimeSlotRecyclerViewAdapter;
import org.pma.fitnessmatcher.api.TimeSlotApi;
import org.pma.fitnessmatcher.model.TimeSlot;
import org.pma.fitnessmatcher.viewmodel.MessageVM;
import org.pma.fitnessmatcher.viewmodel.TimeSlotVM;
import org.pma.fitnessmatcher.viewmodel.ViewModelProviderFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import lombok.NonNull;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnTraineeTimeSlotItemInteraction}
 * interface.
 */
public class TimeSlotFragment extends DaggerFragment {

    private static final String ARG_COLUMN_COUNT = "column-count";
    private int mColumnCount = 1;
    private OnTraineeTimeSlotItemInteraction mListener;

    @Inject
    protected UserSessionManager userSessionManager;

    @Inject
    ViewModelProviderFactory providerFactory;

    TimeSlotRecyclerViewAdapter adapter;

    private TimeSlotVM viewModel;

    @Inject
    protected Retrofit retrofit;
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public TimeSlotFragment() {
    }

    public static TimeSlotFragment newInstance(int columnCount) {
        TimeSlotFragment fragment = new TimeSlotFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.time_slot_list, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.traineeTimeslots);
        if (recyclerView != null) {
            Context context = recyclerView.getContext();
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            adapter = new TimeSlotRecyclerViewAdapter(new ArrayList<TimeSlot>(), mListener);
            recyclerView.setAdapter(adapter);

            viewModel = ViewModelProviders.of(this, providerFactory).get(TimeSlotVM.class);

            subscribe();
        }
        return view;
    }

    private void subscribe() {
        viewModel.getAllByTrainee().observe(getViewLifecycleOwner(), timeSlots -> timeSlots.forEach(t -> adapter.addTimeSlot(t)));
        viewModel.getAllByTraineeStatus().observe(getViewLifecycleOwner(), status -> {
            if(status.equals(TimeSlotVM.Status.ERROR)) {
                Toast toast = Toast.makeText(getContext(), getResources().getString(R.string.time_slots_load_fail), getResources().getInteger(R.integer.duration));
                toast.setGravity(Gravity.TOP|Gravity.RIGHT, getResources().getInteger(R.integer.toast_xoffset), getResources().getInteger(R.integer.toast_yoffset));
                toast.show();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnTraineeTimeSlotItemInteraction) {
            mListener = (OnTraineeTimeSlotItemInteraction) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnTrainerChooserInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnTraineeTimeSlotItemInteraction {
        void onTraineeTimeSlotListInteraction(TimeSlot item);
    }

}
