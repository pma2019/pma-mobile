package org.pma.fitnessmatcher;

import android.content.Context;

import org.pma.fitnessmatcher.component.DaggerAppComponent;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;

import static android.Manifest.permission.GET_ACCOUNTS;
import static android.Manifest.permission.READ_CONTACTS;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by Belal on 12/2/2017.
 */

public class MyApplication extends DaggerApplication {
    private static MyApplication myApp;

    public static final String[] PERMISSIONS = {READ_CONTACTS, READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE, GET_ACCOUNTS};


    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().application(this).build();
    }

    public static Context getContext() {
        return myApp;
    }

    @Override
    public void onCreate() {
        myApp = this;
        super.onCreate();
    }


}