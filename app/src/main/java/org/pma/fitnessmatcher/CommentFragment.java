package org.pma.fitnessmatcher;

import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.pma.fitnessmatcher.adapter.CommentRecyclerViewAdapter;
import org.pma.fitnessmatcher.listeners.OnCommentListInteractionListener;
import org.pma.fitnessmatcher.model.Comment;
import org.pma.fitnessmatcher.viewmodel.CommentVM;
import org.pma.fitnessmatcher.viewmodel.ViewModelProviderFactory;

import java.util.ArrayList;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import retrofit2.Retrofit;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnCommentListInteractionListener}
 * interface.
 */
public class CommentFragment extends DaggerFragment implements OnCommentListInteractionListener {

    public static final String ARG_COLUMN_COUNT = "column-count";
    public static final String TIME_SLOT_ID = "time-slot-id";
    private EditText commentBox;
    private int mColumnCount = 1;

    CommentRecyclerViewAdapter adapter;

    @Inject
    protected UserSessionManager userSessionManager;

    @Inject
    protected Retrofit retrofit;

    @Inject
    ViewModelProviderFactory providerFactory;

    RatingBar ratingBar;

    private CommentVM viewModel;

    private LinearLayout boxHolder;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public CommentFragment() {
    }

    public static CommentFragment newInstance(int columnCount, Integer timeSlotId) {
        CommentFragment fragment = new CommentFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        args.putInt(TIME_SLOT_ID, timeSlotId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_comment_list, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.comment_list);
        Context context = recyclerView.getContext();

        ratingBar = view.findViewById(R.id.rating_bar);
        if (mColumnCount <= 1) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
        }
        adapter = new CommentRecyclerViewAdapter(new ArrayList<Comment>(), this, userSessionManager);
        recyclerView.setAdapter(adapter);
        viewModel = ViewModelProviders.of(this, providerFactory).get(CommentVM.class);


        if(userSessionManager.isTrainee()) {
            subscribeTrainee();
            ratingBar.setOnRatingBarChangeListener((ratingBar, rating, fromUser) -> {
                if(fromUser) {
                    viewModel.addRatings(rating, getArguments().getInt(TIME_SLOT_ID), userSessionManager.getUserDetails().getEmail())
                            .observe(getViewLifecycleOwner(), ratings -> {
                                if(ratings.getId() != null) {
                                    ratingBar.setRating(ratings.getRatingNumber());
                                    ratingBar.setEnabled(false);
                                }
                            });
                }
            });
        } else {
            subscribeTrainer();
        }
        subscribeComments();

        commentBox = view.findViewById(R.id.comment_box);

        Button sendButton = view.findViewById(R.id.button_chatbox_send);
        sendButton.setOnClickListener(v -> viewModel.addComment(userSessionManager.isTrainer(),
                commentBox.getText().toString(),
                getArguments().getInt(TIME_SLOT_ID),
                userSessionManager.getUserDetails().getEmail())
                .observe(getViewLifecycleOwner(), comment -> {
                    if(comment.getId() != null) {
                        adapter.addComment(comment);
                    }
                }));
        boxHolder = view.findViewById(R.id.box_holder);

        return view;
    }

    private void subscribeComments() {
        viewModel.getAllCommentsByTimeSlot().observe(getViewLifecycleOwner(), comments -> {
            for(Comment comment : comments) {
                if (comment.getCreateByEmail().equals(userSessionManager.getUserDetails().getEmail())) {
                    boxHolder.setVisibility(View.INVISIBLE);
                    break;
                }
            }
            for(Comment comment : comments) {
                adapter.addComment(comment);
                if(comment.getReply() != null)
                    adapter.addComment(comment.getReply());
            }
        });
        viewModel.getAllCommentsByTimeSlotStatus().observe(getViewLifecycleOwner(), status -> {
            switch (status) {
                case ERROR:
                    Toast toast = Toast.makeText(getContext(), getResources().getString(R.string.comment_load_fail), getResources().getInteger(R.integer.duration));
                    toast.setGravity(Gravity.TOP|Gravity.RIGHT, getResources().getInteger(R.integer.toast_xoffset), getResources().getInteger(R.integer.toast_yoffset));
                    toast.show();
                    break;
            }
        });
        viewModel.getAddCommentStatus().observe(getViewLifecycleOwner(), status -> {
            if(status.equals(CommentVM.Status.ERROR)) {
                Toast toast = Toast.makeText(getContext(), getResources().getString(R.string.comment_add_fail), getResources().getInteger(R.integer.duration));
                toast.setGravity(Gravity.TOP|Gravity.RIGHT, getResources().getInteger(R.integer.toast_xoffset), getResources().getInteger(R.integer.toast_yoffset));
                toast.show();
            }
        });
    }

    private void subscribeTrainee() {
        viewModel.getRatingByTrainee().observe(getViewLifecycleOwner(), ratings -> {
            ratingBar.setRating(ratings.getRatingNumber());
            ratingBar.setEnabled(false);
        });

        viewModel.getStatusByTrainee().observe(getViewLifecycleOwner(), status -> {
            switch (status) {
                case NOT_FOUND:
                    ratingBar.setEnabled(true);
                    break;
                case ERROR:
                    Toast toast = Toast.makeText(getContext(), getResources().getString(R.string.rating_load_fail), getResources().getInteger(R.integer.duration));
                    toast.setGravity(Gravity.TOP|Gravity.RIGHT, getResources().getInteger(R.integer.toast_xoffset), getResources().getInteger(R.integer.toast_yoffset));
                    toast.show();
                    break;
            }
        });

        viewModel.getAddRatingStatus().observe(getViewLifecycleOwner(), status -> {
            if(status.equals(CommentVM.Status.ERROR)) {
                Toast toast = Toast.makeText(getContext(), getResources().getString(R.string.rating_add_fail), getResources().getInteger(R.integer.duration));
                toast.setGravity(Gravity.TOP|Gravity.RIGHT, getResources().getInteger(R.integer.toast_xoffset), getResources().getInteger(R.integer.toast_yoffset));
                toast.show();
            }
        });
    }

    private void subscribeTrainer() {
        viewModel.getStatusByTrainer().observe(getViewLifecycleOwner(), status -> {
            switch (status) {
                case NOT_FOUND:
                    ratingBar.setEnabled(false);
                    break;
                case ERROR:
                    Toast toast = Toast.makeText(getContext(), getResources().getString(R.string.rating_load_fail), getResources().getInteger(R.integer.duration));
                    toast.setGravity(Gravity.TOP|Gravity.RIGHT, getResources().getInteger(R.integer.toast_xoffset), getResources().getInteger(R.integer.toast_yoffset));
                    toast.show();
                    break;
            }
        });
        viewModel.getRatingByTrainer().observe(getViewLifecycleOwner(), overAllRating -> {
            if(overAllRating.getOverAllRating() != null) {
                ratingBar.setRating(overAllRating.getOverAllRating());
                ratingBar.setEnabled(false);
            }
        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onCommentInteraction(Comment item) {
        boxHolder.setVisibility(View.VISIBLE);
        String text = "@" + item.getCreateByUsername()+ " ";
        commentBox.setText(text, TextView.BufferType.EDITABLE);
        if(commentBox.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
        commentBox.setSelection(text.length());
    }

}
