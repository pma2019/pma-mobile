package org.pma.fitnessmatcher;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;

import org.pma.fitnessmatcher.adapter.TrainerImageRecyclerViewAdapter;
import org.pma.fitnessmatcher.model.Document;
import org.pma.fitnessmatcher.model.TrainerProfile;
import org.pma.fitnessmatcher.viewmodel.TrainerProfileVM;
import org.pma.fitnessmatcher.viewmodel.ViewModelProviderFactory;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import lombok.Setter;
import retrofit2.Retrofit;

import static android.app.Activity.RESULT_OK;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class TrainerProfileFragment extends DaggerFragment {

    public static final String ARG_COLUMN_COUNT = "column-count";
    public static final String ARG_TRAINER_EMAIL = "email";
    public static final String ARG_TRAINER_USERNAME = "username";
    public static final String VIEWING_AS_TRAINER = "view";
    private int mColumnCount = 2;
    private RecyclerView recyclerView;
    @Setter
    private TrainerProfile trainerProfile;
    private OnListFragmentInteractionListener mListener;
    private TrainerImageRecyclerViewAdapter adapter;

    @Inject
    protected UserSessionManager userSessionManager;

    @Inject
    protected Retrofit retrofit;

    @Inject
    ViewModelProviderFactory providerFactory;

    @Inject
    TrainerProfileVM viewModel;

    private TextView gymName;
    private TextView name;
    private TextView ratings;
    private ImageView profile_picture;
    private TextView num_photos;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public TrainerProfileFragment() {

    }

    public static TrainerProfileFragment newInstance(int columnCount, String email, String username, boolean trainer) {
        TrainerProfileFragment fragment = new TrainerProfileFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        args.putString(ARG_TRAINER_EMAIL, email);
        args.putString(ARG_TRAINER_USERNAME, username);
        args.putBoolean(VIEWING_AS_TRAINER, trainer);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.trainer_profile_list, container, false);


        gymName = (TextView) view.findViewById(R.id.gymName);
        name = (TextView) view.findViewById(R.id.name);
        ratings = view.findViewById(R.id.rating_holder);
        profile_picture = view.findViewById(R.id.profile_picture);

        // add new picture button
        FloatingActionButton newPicBtn = view.findViewById(R.id.newPicBtn);
        newPicBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final CharSequence[] options = { "Take Photo", "Choose from Gallery", "Cancel"};

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Add Photo!");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals("Take Photo")) {
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, 1);
                        } else if (options[item].equals("Choose from Gallery")) {
                            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                            photoPickerIntent.setType("image/*");
                            startActivityForResult(photoPickerIntent, 2);
                        } else if (options[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            }
        });

        Button bookAppointment = view.findViewById(R.id.book_appointment);
        bookAppointment.setOnClickListener(v -> {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, AppointmentFragment
                            .newInstance(1, getArguments().getString(ARG_TRAINER_EMAIL), getArguments().getString(ARG_TRAINER_USERNAME)))
                    .addToBackStack("trainer-appointment-booking")
                    .commit();
        });

        if(userSessionManager.isTrainer())
            bookAppointment.setVisibility(View.INVISIBLE);

        recyclerView = view.findViewById(R.id.trainer_photos);
        num_photos = view.findViewById(R.id.num_photos);
        if (recyclerView != null) {
            Context context = recyclerView.getContext();
            if (mColumnCount <= 1) {
                LinearLayoutManager manager = new LinearLayoutManager(context);
                recyclerView.setLayoutManager(manager);
            } else {
                GridLayoutManager manager = new GridLayoutManager(context, mColumnCount);
                recyclerView.setLayoutManager(manager);
            }
            adapter = new TrainerImageRecyclerViewAdapter(new ArrayList<Document>(), mListener);
            recyclerView.setAdapter(adapter);
        }
        viewModel = ViewModelProviders.of(this, providerFactory).get(TrainerProfileVM.class);
        subscribe();

        return view;
    }

    private void subscribe() {
        viewModel.getTrainer().observe(getViewLifecycleOwner(), trainerProfile -> {
            if(trainerProfile.getId() != null) {
                gymName.setText(trainerProfile.getGymName());
                name.setText(trainerProfile.getUsername());
                ratings.setText(trainerProfile.getOverallRatings() + "");
                if(trainerProfile.getImages() != null) {
                    num_photos.setText(trainerProfile.getImages().size() + "");
                    Picasso.with(getContext()).load(trainerProfile.getProfilePictureStorageUrl()).into(profile_picture);
                    for (int i = 0; i < trainerProfile.getImages().size(); i++) {
                        adapter.addImage(trainerProfile.getImages().get(i), i);
                    }
                }
            }
        });

        viewModel.getTrainerLoadStatus().observe(getViewLifecycleOwner(), status -> {
            if(status.equals(TrainerProfileVM.Status.ERROR)) {
                Toast toast = Toast.makeText(getContext(), getResources().getString(R.string.trainer_load_fail), getResources().getInteger(R.integer.duration));
                toast.setGravity(Gravity.TOP|Gravity.RIGHT, getResources().getInteger(R.integer.toast_xoffset), getResources().getInteger(R.integer.toast_yoffset));
                toast.show();
            }
        });

        viewModel.getAddPictureStatus().observe(getViewLifecycleOwner(), status -> {
            if(status.equals(TrainerProfileVM.Status.ERROR)){
                Toast toast = Toast.makeText(getContext(), getResources().getString(R.string.image_upload_failed), getResources().getInteger(R.integer.duration));
                toast.setGravity(Gravity.TOP|Gravity.RIGHT, getResources().getInteger(R.integer.toast_xoffset), getResources().getInteger(R.integer.toast_yoffset));
                toast.show();
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");

                String path_external = Environment.getExternalStorageDirectory() + File.separator + "_avatar";
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                File imageFile = new File(path_external);
                try {
                    imageFile.createNewFile();
                    FileOutputStream fo = new FileOutputStream(imageFile);
                    fo.write(bytes.toByteArray());
                    fo.flush();
                    fo.close();
                    viewModel.addPicture(imageFile).observe(getViewLifecycleOwner(), document -> {
                        if(document.getId() != null) {
                            adapter.addImage(document, 0);
                            recyclerView.scrollToPosition(0);
                            viewModel.getAddPictureStatus().postValue(TrainerProfileVM.Status.SUCCESS);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (requestCode == 2) {
                Uri selectedImage = data.getData();
                String[] filePath = { MediaStore.Images.Media.DATA };
                Cursor c = getActivity().getContentResolver().query(selectedImage,filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                File imageFile = new File(picturePath);

                viewModel.addPicture(imageFile).observe(getViewLifecycleOwner(), document -> {
                    if(document.getId() != null) {
                        adapter.addImage(document, 0);
                        recyclerView.scrollToPosition(0);
                    }
                });


            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnTrainerChooserInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Document item);
    }

}
