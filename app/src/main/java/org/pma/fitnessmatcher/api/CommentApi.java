package org.pma.fitnessmatcher.api;

import org.pma.fitnessmatcher.api.requests.AddCommentRequest;
import org.pma.fitnessmatcher.model.Comment;

import java.util.List;

import io.reactivex.Flowable;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface CommentApi {

    String BASE_URL = "comments/";

    @GET(BASE_URL + "byTimeSlot")
    Flowable<List<Comment>> getAllByTimeSlot(@Header("Authorization") String bearer, @Query("timeSlotId") Integer id);

    @POST(BASE_URL + "addComment")
    Flowable<Comment> addComment(@Header("Authorization") String bearer, @Body AddCommentRequest newComment);


}
