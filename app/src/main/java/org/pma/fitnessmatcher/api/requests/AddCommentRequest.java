package org.pma.fitnessmatcher.api.requests;

import com.google.gson.annotations.Expose;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class AddCommentRequest {
    @Expose
    private Integer timeSlotId;
    @Expose
    private String body;
    @Expose
    private String createByEmail;
    @Expose
    private Boolean byTrainer;
}
