package org.pma.fitnessmatcher.api.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.pma.fitnessmatcher.model.UserProfile;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class LoginResponse {
    @Expose
    @SerializedName("userProfile")
    private UserProfile userProfile;

    @Expose
    @SerializedName("roles")
    private List<String> roles;

    @Expose
    @SerializedName("token")
    private String token;


}
