package org.pma.fitnessmatcher.api.requests;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class AddRatingRequest {
    private Float ratingNumber;
    private String traineeEmail;
    private Integer timeSlotId;
}