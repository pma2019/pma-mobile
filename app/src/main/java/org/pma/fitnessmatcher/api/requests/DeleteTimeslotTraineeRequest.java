package org.pma.fitnessmatcher.api.requests;

import com.google.gson.annotations.Expose;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter@Getter@AllArgsConstructor@NoArgsConstructor
public class DeleteTimeslotTraineeRequest {
    @Expose
    private String email;
    @Expose
    private Integer timeSlotId;
}
