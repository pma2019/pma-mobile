package org.pma.fitnessmatcher.api;

import org.pma.fitnessmatcher.api.requests.AddMessageRequest;
import org.pma.fitnessmatcher.model.Message;

import java.util.List;

import io.reactivex.Flowable;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface MessagesApi {

    String BASE_URL = "chat/";

    @GET(BASE_URL + "getInbox")
    Flowable<List<Message>> getInbox(@Header("Authorization") String value);

    @GET(BASE_URL + "getMessagesFrom")
    Flowable<List<Message>> getMessagesFrom(@Header("Authorization") String value, @Query("email") String email);

    @POST(BASE_URL + "sendMessage")
    Flowable<Message> addMessageFlow(@Header("Authorization") String value, @Body AddMessageRequest request);
}
