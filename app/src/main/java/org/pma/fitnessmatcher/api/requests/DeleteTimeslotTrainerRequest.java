package org.pma.fitnessmatcher.api.requests;

import com.google.gson.annotations.Expose;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter@Getter@NoArgsConstructor@AllArgsConstructor
public class DeleteTimeslotTrainerRequest {
    @Expose
    private String trainerEmail;
    @Expose
    private Integer timeSlotId;
}
