package org.pma.fitnessmatcher.api.responses;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class OverAllRating {
    private Integer timeSlotId;
    private String trainerEmail;
    private Float overAllRating;
}
