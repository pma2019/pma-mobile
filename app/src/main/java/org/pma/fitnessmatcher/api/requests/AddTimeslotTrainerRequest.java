package org.pma.fitnessmatcher.api.requests;

import com.google.gson.annotations.Expose;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter@Setter@AllArgsConstructor@NoArgsConstructor
public class AddTimeslotTrainerRequest {
    @Expose
    private String trainerEmail;
    @Expose
    private Integer year;
    @Expose
    private Integer month;
    @Expose
    private Integer day;
    @Expose
    private Integer hour;

}
