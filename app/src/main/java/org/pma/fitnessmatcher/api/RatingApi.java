package org.pma.fitnessmatcher.api;

import org.pma.fitnessmatcher.api.requests.AddRatingRequest;
import org.pma.fitnessmatcher.api.responses.OverAllRating;
import org.pma.fitnessmatcher.model.Ratings;

import io.reactivex.Flowable;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface RatingApi {

    String BASE_URL = "ratings/";

    @GET(BASE_URL + "findByTraineeTimeSlot")
    Flowable<Ratings> findByTraineeAndTimeSlot(@Header("Authorization") String value, @Query("traineeEmail") String email, @Query("timeSlotId") Integer id);

    @GET(BASE_URL + "findByTrainerTimeSlot")
    Flowable<OverAllRating> findByTrainerAndTimeSlot(@Header("Authorization") String value, @Query("trainerEmail") String email, @Query("timeSlotId") Integer id);

    @POST(BASE_URL + "addRating")
    Flowable<Ratings> addRating(@Header("Authorization") String value, @Body AddRatingRequest rating);
}
