package org.pma.fitnessmatcher.api.requests;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class PasswordChange {
    private String email;
    private String oldPassword;
    private String newPassword;
}
