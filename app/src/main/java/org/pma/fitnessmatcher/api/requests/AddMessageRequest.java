package org.pma.fitnessmatcher.api.requests;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AddMessageRequest {
    private String senderEmail;
    private String receiverEmail;
    private String text;
}
