package org.pma.fitnessmatcher.api;

import org.pma.fitnessmatcher.api.requests.LoginRequest;
import org.pma.fitnessmatcher.api.requests.PasswordChange;
import org.pma.fitnessmatcher.api.requests.RegisterRequest;
import org.pma.fitnessmatcher.api.responses.LoginResponse;
import org.pma.fitnessmatcher.model.TrainerProfile;
import org.pma.fitnessmatcher.model.UserProfile;

import java.util.List;

import io.reactivex.Flowable;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface UserApi {
    @POST("auth/login")
    Call<LoginResponse> login(@Body LoginRequest request);

    @POST("users/register")
    Call<Boolean> register(@Body RegisterRequest request);

    @GET("users/getTrainers")
    Flowable<List<TrainerProfile>> getTrainers(@Header("Authorization") String bearer);

    @GET("users/getTrainer")
    Flowable<TrainerProfile> getTrainer(@Header("Authorization") String bearer, @Query("email") String email);

    @POST
    Flowable<UserProfile> changePassword(@Header("Authorization") String bearer, @Body PasswordChange request);
}
