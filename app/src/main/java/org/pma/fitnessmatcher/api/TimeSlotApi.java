package org.pma.fitnessmatcher.api;


import org.pma.fitnessmatcher.api.requests.MakeAppointmentRequest;
import org.pma.fitnessmatcher.api.requests.AddTimeslotTrainerRequest;
import org.pma.fitnessmatcher.api.requests.DeleteTimeslotTraineeRequest;
import org.pma.fitnessmatcher.api.requests.DeleteTimeslotTrainerRequest;
import org.pma.fitnessmatcher.model.TimeSlot;

import java.util.List;

import io.reactivex.Flowable;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface TimeSlotApi {
    String BASE_URL = "appointments/";

    @GET(BASE_URL + "byTrainee")
    Flowable<List<TimeSlot>> getAllByTrainee(@Header("Authorization")String header, @Query("email") String traineeEmail);

    @GET(BASE_URL + "byTrainer")
    Flowable<List<TimeSlot>> getAllByTrainer(@Header("Authorization")String header, @Query("email") String trainerEmail);

    @POST(BASE_URL+"addtimeslottrainer")
    Call<TimeSlot> addTimeslotTrainer(@Body AddTimeslotTrainerRequest request);

    @POST(BASE_URL+"addtimeslottrainee")
    Flowable<TimeSlot> addTimeslotTrainee(@Header("Authorization")String header, @Body MakeAppointmentRequest request);

    @DELETE(BASE_URL+"deleteTimeslotTrainer")
    Call<TimeSlot>  deleteTimeslotTrainer(@Body DeleteTimeslotTrainerRequest request);

    @DELETE(BASE_URL+"deleteTimeslotTrainee")
    Call<TimeSlot>  deleteTimeslotTrainee(@Body DeleteTimeslotTraineeRequest request);

}
