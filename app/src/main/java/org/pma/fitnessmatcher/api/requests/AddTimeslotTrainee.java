package org.pma.fitnessmatcher.api.requests;

import com.google.gson.annotations.Expose;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AddTimeslotTrainee {
    @Expose
    private String email;
    @Expose
    private Integer timeSlotId;
}
