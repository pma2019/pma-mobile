package org.pma.fitnessmatcher.api;

import org.pma.fitnessmatcher.model.Document;
import org.pma.fitnessmatcher.model.TrainerProfile;
import org.pma.fitnessmatcher.model.UserProfile;

import io.reactivex.Flowable;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface DocumentApi {

    @Multipart
    @POST("documents/addpicture")
    Flowable<Document> changeAvatar(@Header("Authorization") String value,
                                    @Part MultipartBody.Part filePart,
                                    @Part MultipartBody.Part email,
                                    @Part MultipartBody.Part isAvatar);

    @Multipart
    @POST("documents/addpicture")
    Flowable<Document> addPicture(@Header("Authorization") String value,
                                  @Part MultipartBody.Part filePart,
                                  @Part MultipartBody.Part email,
                                  @Part MultipartBody.Part isAvatar);

}
