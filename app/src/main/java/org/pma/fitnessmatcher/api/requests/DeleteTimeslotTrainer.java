package org.pma.fitnessmatcher.api.requests;

import com.google.gson.annotations.Expose;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class DeleteTimeslotTrainer {
    @Expose
    private String trainerEmail;
    @Expose
    private Integer timeSlotId;
}
