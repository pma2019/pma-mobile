package org.pma.fitnessmatcher;

import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.pma.fitnessmatcher.adapter.MessageRecyclerViewAdapter;
import org.pma.fitnessmatcher.model.Message;
import org.pma.fitnessmatcher.viewmodel.MessageVM;
import org.pma.fitnessmatcher.viewmodel.ViewModelProviderFactory;

import java.util.ArrayList;
import java.util.Date;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import retrofit2.Retrofit;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class MessageFragment extends DaggerFragment {

    public static final String ARG_COLUMN_COUNT = "column-count";
    public static final String CHATTER_EMAIL = "chatter_email";
    public static final String CHATTER_PROFILE_PICTURE = "chatter_picture";
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;

    @Inject
    protected UserSessionManager userSessionManager;

    @Inject
    protected Retrofit retrofit;

    @Inject
    ViewModelProviderFactory providerFactory;

    MessageVM viewModel;

    MessageRecyclerViewAdapter adapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public MessageFragment() {
    }


    public static MessageFragment newInstance(int columnCount, String chatter, String chatterPicture) {
        MessageFragment fragment = new MessageFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        args.putString(CHATTER_EMAIL, chatter);
        args.putString(CHATTER_PROFILE_PICTURE, chatterPicture);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_message_list, container, false);
        Toolbar toolbar = view.findViewById(R.id.message_toolbar);
        toolbar.setTitle(getArguments().getString(CHATTER_EMAIL));
        final RecyclerView recyclerView = view.findViewById(R.id.message_list);

        Context context = recyclerView.getContext();
        if (mColumnCount <= 1) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
        }
        adapter = new MessageRecyclerViewAdapter(new ArrayList<Message>(), mListener, userSessionManager);
        recyclerView.setAdapter(adapter);
        recyclerView.scrollToPosition(adapter.getItemCount() - 1);

        Button sendButton = view.findViewById(R.id.button_chatbox_send);
        sendButton.setOnClickListener(v -> {
            EditText chatBox = view.findViewById(R.id.chatBox);
            String text = chatBox.getText().toString();
            viewModel.sendMessage(userSessionManager.getUserDetails().getEmail(), getArguments().getString(CHATTER_EMAIL), text).observe(getViewLifecycleOwner(), returnMsg -> {
                if(returnMsg.getId() != null) {
                    adapter.addMessage(returnMsg);
                }
            });
        });
        viewModel = ViewModelProviders.of(this, providerFactory).get(MessageVM.class);

        subscribe();

        return view;
    }
    private void subscribe() {
        viewModel.getGetFromStatus().observe(getViewLifecycleOwner(), status -> {
            if (status.equals(MessageVM.Status.ERROR)){
                Toast toast = Toast.makeText(getContext(), getResources().getString(R.string.chat_load_fail), getResources().getInteger(R.integer.duration));
                toast.setGravity(Gravity.TOP | Gravity.RIGHT, getResources().getInteger(R.integer.toast_xoffset), getResources().getInteger(R.integer.toast_yoffset));
                toast.show();
            }
        });

        viewModel.getFromMessages()
                .observe(getViewLifecycleOwner(), messages -> messages.forEach(m -> {
                    if(m != null && m.getId() != null) {
                        adapter.addMessage(m);
                    }
                }));

        viewModel.getGetFromStatus().observe(getViewLifecycleOwner(), status -> {
            if(status.equals(MessageVM.Status.ERROR)) {
                Toast toast = Toast.makeText(getContext(), getResources().getString(R.string.chat_new_fail), getResources().getInteger(R.integer.duration));
                toast.setGravity(Gravity.TOP|Gravity.RIGHT, getResources().getInteger(R.integer.toast_xoffset), getResources().getInteger(R.integer.toast_yoffset));
                toast.show();
            }
        });

        viewModel.getMessages().observe(getViewLifecycleOwner(), message -> {
            if(message.getId() != null)
                adapter.addMessage(message);
        });
        viewModel.getMessagesStatus().observe(getViewLifecycleOwner(), status -> {
            if(status.equals(MessageVM.Status.ERROR)) {
                Toast toast = Toast.makeText(getContext(), getResources().getString(R.string.chat_load_new), getResources().getInteger(R.integer.duration));
                toast.setGravity(Gravity.TOP|Gravity.RIGHT, getResources().getInteger(R.integer.toast_xoffset), getResources().getInteger(R.integer.toast_yoffset));
                toast.show();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnTrainerChooserInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Message item);
    }


}
