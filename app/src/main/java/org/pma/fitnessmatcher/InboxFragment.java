package org.pma.fitnessmatcher;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.pma.fitnessmatcher.adapter.InboxRecyclerViewAdapter;
import org.pma.fitnessmatcher.api.MessagesApi;
import org.pma.fitnessmatcher.model.Message;
import org.pma.fitnessmatcher.viewmodel.InboxVM;
import org.pma.fitnessmatcher.viewmodel.ViewModelProviderFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListInboxInteractionListener}
 * interface.
 */
public class InboxFragment extends DaggerFragment {

    private static final String ARG_COLUMN_COUNT = "column-count";
    private int mColumnCount = 1;
    private OnListInboxInteractionListener mListener;

    InboxVM viewModel;

    @Inject
    ViewModelProviderFactory providerFactory;

    InboxRecyclerViewAdapter adapter;

    @Inject
    protected UserSessionManager userSessionManager;

    @Inject
    protected Retrofit retrofit;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public InboxFragment() {
    }

    public static InboxFragment newInstance(int columnCount) {
        InboxFragment fragment = new InboxFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
        viewModel = ViewModelProviders.of(this, providerFactory).get(InboxVM.class);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_inbox_list, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.inbox_list);

        Context context = recyclerView.getContext();
        if (mColumnCount <= 1) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
        }
        adapter = new InboxRecyclerViewAdapter(new ArrayList<Message>(), mListener);
        recyclerView.setAdapter(adapter);

        subscribe();

        return view;
    }

    private void subscribe() {
        viewModel.getGetInboxStatus().observe(getViewLifecycleOwner(), status -> {
            Toast toast = Toast.makeText(getContext(), getResources().getString(R.string.inbox_load_failure), getResources().getInteger(R.integer.duration));
            toast.setGravity(Gravity.TOP|Gravity.RIGHT, getResources().getInteger(R.integer.toast_xoffset), getResources().getInteger(R.integer.toast_yoffset));
            toast.show();
        });

        viewModel.getInbox().observe(getViewLifecycleOwner(), messages -> {
            messages.forEach(m -> adapter.addMessage(m));
        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListInboxInteractionListener) {
            mListener = (OnListInboxInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnTrainerChooserInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListInboxInteractionListener {
        void onListInboxInteractionListener(Message item);
    }
}
