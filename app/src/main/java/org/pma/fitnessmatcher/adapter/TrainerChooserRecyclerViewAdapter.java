package org.pma.fitnessmatcher.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.pma.fitnessmatcher.R;
import org.pma.fitnessmatcher.TrainerChooserFragment;
import org.pma.fitnessmatcher.model.TrainerProfile;

import java.util.List;

/**
 *
 *
 */
public class TrainerChooserRecyclerViewAdapter extends RecyclerView.Adapter<TrainerChooserRecyclerViewAdapter.ViewHolder> {

    private final List<TrainerProfile> tValues;
    private final TrainerChooserFragment.OnTrainerChooserInteractionListener tListener;

    public TrainerChooserRecyclerViewAdapter(List<TrainerProfile> items, TrainerChooserFragment.OnTrainerChooserInteractionListener listener) {
        tValues = items;
        tListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.trainer_chooser_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final TrainerProfile item = tValues.get(position);
        holder.bind(item);
        holder.trainerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(null!=tListener){
                    tListener.onTrainerChooserItemTouch(item);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return tValues.size();
    }

    public void addTrainer(TrainerProfile trainerProfile) {
        tValues.add(trainerProfile);
        notifyItemInserted(getItemCount() - 1);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View trainerView;
        public final TextView trainerNameView;
        public final ImageView trainerAvatarView;
        public final TextView trainerGymNameView;
        public final TextView trainerRatingsView;
        public TrainerProfile trainerItem;

        public ViewHolder(View view) {
            super(view);
            trainerView = view;
            trainerNameView = view.findViewById(R.id.name);
            trainerAvatarView = view.findViewById(R.id.avatar);
            trainerGymNameView = view.findViewById(R.id.gymName);
            trainerRatingsView = view.findViewById(R.id.rating);
        }
        void bind(TrainerProfile trainerProfile){
            trainerItem=trainerProfile;
            trainerNameView.setText(trainerProfile.getLastName()+" "+trainerProfile.getName());
            Picasso.with(trainerView.getContext()).load(trainerProfile.getProfilePictureStorageUrl()).into(trainerAvatarView);
            trainerGymNameView.setText(trainerProfile.getUsername());
            trainerRatingsView.setText(trainerProfile.getOverallRatings().toString());
        }

        @Override
        public String toString() {
            return super.toString() + " '" + trainerItem.getName() + "'";
        }
    }
}
