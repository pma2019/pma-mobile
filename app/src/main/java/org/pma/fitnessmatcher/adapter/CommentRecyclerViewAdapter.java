package org.pma.fitnessmatcher.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import org.pma.fitnessmatcher.R;
import org.pma.fitnessmatcher.UserSessionManager;
import org.pma.fitnessmatcher.listeners.OnCommentListInteractionListener;
import org.pma.fitnessmatcher.model.Comment;

import java.util.List;

import javax.inject.Inject;

/**
 * {@link RecyclerView.Adapter} that can display a
 * specified {@link OnCommentListInteractionListener}.
 */
public class CommentRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final List<Comment> mValues;
    private final OnCommentListInteractionListener mListener;
    private final int MAIN_COMMENT = 0;
    private final int REPLY_COMMENT = 1;

    private UserSessionManager userSessionManager;

    public CommentRecyclerViewAdapter(List<Comment> items, OnCommentListInteractionListener listener, UserSessionManager userSessionManager) {
        mValues = items;
        mListener = listener;
        this.userSessionManager = userSessionManager;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        if(viewType == MAIN_COMMENT) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_comment_item, parent, false);
            return new MainViewHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_comment_item_response, parent, false);
            return new ReplyViewHolder(view);
        }

    }

    @Override
    public int getItemViewType(int position) {
        Comment comment = (Comment) mValues.get(position);
        if (!comment.getByTrainer()){
            return MAIN_COMMENT;
        } else {
            return REPLY_COMMENT;
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder.getItemViewType() == MAIN_COMMENT) {
            final MainViewHolder mainViewHolder = (MainViewHolder) holder;
            mainViewHolder.mItem = mValues.get(position);
            mainViewHolder.mUsernameView.setText(mValues.get(position).getCreateByUsername());
            mainViewHolder.mBodyView.setText(mValues.get(position).getBody());

            if(userSessionManager.isTrainee()) {
                mainViewHolder.mReplyView.setVisibility(View.INVISIBLE);
            }
            mainViewHolder.mReplyView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onCommentInteraction(mainViewHolder.mItem);
                }
            });
            Picasso.with(mainViewHolder.mView.getContext()).load(mainViewHolder.mItem.getProfilePicture()).into(mainViewHolder.mImageView);
        } else {
            ReplyViewHolder replyViewHolder = (ReplyViewHolder) holder;
            replyViewHolder.mItem = mValues.get(position);
            replyViewHolder.mUsernameView.setText(mValues.get(position).getCreateByUsername());
            replyViewHolder.mBodyView.setText(mValues.get(position).getBody());
            Picasso.with(replyViewHolder.mView.getContext()).load(replyViewHolder.mItem.getProfilePicture()).into(replyViewHolder.mImageView);
        }


    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void addComment(Comment comment) {
        mValues.add(comment);
        notifyItemChanged(getItemCount() - 1);
    }

    class ReplyViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mImageView;
        public final TextView mUsernameView;
        public final TextView mBodyView;
        public Comment mItem;

        public ReplyViewHolder(View view) {
            super(view);
            mView = view;
            mImageView =  view.findViewById(R.id.profile_picture);
            mBodyView =  view.findViewById(R.id.message_body);
            mUsernameView = view.findViewById(R.id.name);
        }
    }
    class MainViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mImageView;
        public final TextView mUsernameView;
        public final TextView mBodyView;
        public final TextView  mReplyView;
        public Comment mItem;
        public MainViewHolder(View view) {
            super(view);
            mView = view;
            mImageView =  view.findViewById(R.id.profile_picture);
            mBodyView =  view.findViewById(R.id.message_body);
            mUsernameView = view.findViewById(R.id.name);
            mReplyView = view.findViewById(R.id.reply);
        }
    }

}
