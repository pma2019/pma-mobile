package org.pma.fitnessmatcher.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import org.pma.fitnessmatcher.InboxFragment;
import org.pma.fitnessmatcher.R;
import org.pma.fitnessmatcher.model.Message;

import java.util.List;


/**
 *
 *
 */

public class InboxRecyclerViewAdapter extends RecyclerView.Adapter<InboxRecyclerViewAdapter.ViewHolder> {

    private final List<Message> mValues;
    private final InboxFragment.OnListInboxInteractionListener mListener;

    public InboxRecyclerViewAdapter(List<Message> items, InboxFragment.OnListInboxInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    public void setMValues(List<Message> mValues) {
        mValues.addAll(mValues);
        notifyDataSetChanged();
    }

    public void addMessage(Message message) {
        mValues.add(message);
        notifyItemInserted(getItemCount() - 1);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_inbox_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Message message = mValues.get(position);
        holder.mItem = message;
        holder.mUsernameView.setText(message.getSenderUsername());
        if(message.getBody().length() > 30)
            holder.mBodyView.setText(message.getBody().substring(0, 30) + "...");
        else
            holder.mBodyView.setText(message.getBody());
        Picasso.with(holder.mView.getContext()).load(message.getReceiverProfilePicture()).into(holder.mImageView);
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListInboxInteractionListener(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }




    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mImageView;
        public final TextView mUsernameView;
        public final TextView mBodyView;
        public Message mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mImageView =  view.findViewById(R.id.profile_picture);
            mBodyView =  view.findViewById(R.id.message_body);
            mUsernameView = view.findViewById(R.id.name);
        }

    }
}
