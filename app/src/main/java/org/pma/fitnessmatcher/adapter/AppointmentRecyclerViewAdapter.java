package org.pma.fitnessmatcher.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import org.pma.fitnessmatcher.R;
import org.pma.fitnessmatcher.listeners.OnAppointmentItemInteractionListener;
import org.pma.fitnessmatcher.model.TimeSlot;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display and makes a call to the
 * specified {@link OnAppointmentItemInteractionListener}.
 *
 */
public class AppointmentRecyclerViewAdapter extends RecyclerView.Adapter<AppointmentRecyclerViewAdapter.ViewHolder> {

    private final List<TimeSlot> mValues;
    private final OnAppointmentItemInteractionListener mListener;

    public AppointmentRecyclerViewAdapter(List<TimeSlot> items, OnAppointmentItemInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.appointment_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final TimeSlot item = mValues.get(position);
        holder.bind(item);
        holder.mSchedule.setOnClickListener(v -> {
            mListener.onListFragmentInteraction(item);
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void addAppointment(TimeSlot timeSlot) {
        this.mValues.add(timeSlot);
        notifyItemInserted(getItemCount() - 1);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mNameView;
        public final ImageView mAvatarView;
        public final TextView mGymNameView;
        public final TextView mTimeView;
        public final ImageButton mSchedule;
        public TimeSlot mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mNameView = view.findViewById(R.id.name);
            mAvatarView = view.findViewById(R.id.avatar);
            mGymNameView = view.findViewById(R.id.gymName);
            mTimeView = view.findViewById(R.id.timeAppointment);
            mSchedule = view.findViewById(R.id.schedule);
        }

        void bind(TimeSlot timeSlot) {
            mItem = timeSlot;
            mNameView.setText("trainer: "+timeSlot.getTrainerName());
            Picasso.with(mView.getContext()).load(timeSlot.getTrainerProfilePicture()).into(mAvatarView);
            mGymNameView.setText(timeSlot.getYear()+"-"+timeSlot.getMonth()+"-"+timeSlot.getDay());
            mTimeView.setText(timeSlot.getHour()+":00");
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mNameView.getText() + "'";
        }
    }
}
