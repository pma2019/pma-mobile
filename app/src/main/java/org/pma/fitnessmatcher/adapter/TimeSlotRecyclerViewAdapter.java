package org.pma.fitnessmatcher.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import org.pma.fitnessmatcher.R;
import org.pma.fitnessmatcher.TimeSlotFragment.OnTraineeTimeSlotItemInteraction;
import org.pma.fitnessmatcher.model.TimeSlot;

import java.util.List;

import lombok.Getter;

/**
 *
 *
 */
public class TimeSlotRecyclerViewAdapter extends RecyclerView.Adapter<TimeSlotRecyclerViewAdapter.ViewHolder> {
    @Getter
    private final List<TimeSlot> mValues;
    private final OnTraineeTimeSlotItemInteraction mListener;

    public TimeSlotRecyclerViewAdapter(List<TimeSlot> items, OnTraineeTimeSlotItemInteraction listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.time_slot_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final TimeSlot item = mValues.get(position);
        ViewHolder viewHolder=holder;
        holder.bind(item);
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onTraineeTimeSlotListInteraction(item);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void addTimeSlot(TimeSlot timeSlot) {
        mValues.add(timeSlot);
        notifyItemInserted(getItemCount() - 1);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mNameView;
        public final ImageButton mAvatarView;
        public final TextView mGymNameView;
        public final TextView mTimeView;
        public TimeSlot mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mNameView = view.findViewById(R.id.name);
            mAvatarView = view.findViewById(R.id.avatar);
            mGymNameView = view.findViewById(R.id.gymName);
            mTimeView = view.findViewById(R.id.timeAppointment);
        }

        void bind(TimeSlot timeSlot) {
            mItem = timeSlot;
            mNameView.setText("trainer: "+timeSlot.getTrainerName());
            mAvatarView.setImageResource(R.drawable.ic_delete);
//            Picasso.with(mView.getContext()).load(R.id.ukloni).into(mAvatarView);
            mGymNameView.setText(timeSlot.getYear()+"-"+timeSlot.getMonth()+"-"+timeSlot.getDay());
            mTimeView.setText(timeSlot.getHour()+":00");
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mNameView.getText() + "'";
        }
    }
}
