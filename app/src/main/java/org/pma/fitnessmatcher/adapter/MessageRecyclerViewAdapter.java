package org.pma.fitnessmatcher.adapter;

import androidx.recyclerview.widget.RecyclerView;

import android.icu.text.SimpleDateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.pma.fitnessmatcher.MessageFragment.OnListFragmentInteractionListener;
import org.pma.fitnessmatcher.R;
import org.pma.fitnessmatcher.UserSessionManager;
import org.pma.fitnessmatcher.model.Message;

import java.util.List;

import javax.inject.Inject;

/**
 *
 *
 */
public class MessageRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private static final int VIEW_TYPE_MESSAGE_SENT = 1;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;

    private final List<Message> messageList;
    private final OnListFragmentInteractionListener mListener;

    protected UserSessionManager userSessionManager;

    public MessageRecyclerViewAdapter(List<Message> messages, OnListFragmentInteractionListener listener, UserSessionManager userSessionManager) {
        messageList = messages;
        mListener = listener;
        this.userSessionManager = userSessionManager;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        if (viewType == VIEW_TYPE_MESSAGE_SENT) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_message_list_sent, parent, false);
            return new SentViewHolder(view);
        } else if (viewType == VIEW_TYPE_MESSAGE_RECEIVED) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_message_list_received, parent, false);
            return new ReceivedViewHolder(view);
        }

        return null;
    }

    @Override
    public int getItemViewType(int position) {
        Message message = (Message) messageList.get(position);
        if (message.getSenderUsername().equals(userSessionManager.getUserDetails().getUsername())){
            // If the current user is the sender of the message
            return VIEW_TYPE_MESSAGE_SENT;
        } else {
            // If some other user sent the message
            return VIEW_TYPE_MESSAGE_RECEIVED;
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final Message message =  messageList.get(position);

        switch (holder.getItemViewType()) {
            case VIEW_TYPE_MESSAGE_SENT:
                SentViewHolder sentViewHolder = ((SentViewHolder) holder);
                sentViewHolder.bind(message);
                sentViewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (null != mListener) {
                            // Notify the active callbacks interface (the activity, if the
                            // fragment is attached to one) that an item has been selected.
                            mListener.onListFragmentInteraction(message);
                        }
                    }
                });
                break;
            case VIEW_TYPE_MESSAGE_RECEIVED:
                ReceivedViewHolder receivedViewHolder = ((ReceivedViewHolder) holder);
                receivedViewHolder.bind(message);
                receivedViewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (null != mListener) {
                            // Notify the active callbacks interface (the activity, if the
                            // fragment is attached to one) that an item has been selected.
                            mListener.onListFragmentInteraction(message);
                        }
                    }
                });
        }
    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }

    public void addMessage(Message message) {
        messageList.add(message);
        notifyItemInserted(getItemCount() - 1);
    }

    public class SentViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public Message mItem;
        TextView messageText, timeText;
        ImageView profileImage;

        SentViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            messageText = (TextView) itemView.findViewById(R.id.text_message_body);
            timeText = (TextView) itemView.findViewById(R.id.text_message_time);

        }

        void bind(Message message) {
            mItem = message;
            messageText.setText(message.getBody());

            SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm");
            timeText.setText(dateFormat.format(message.getSentDate()));
        }
    }

    public class ReceivedViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public Message mItem;
        TextView messageText, timeText, nameText;
        ImageView profileImage;

        ReceivedViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            messageText = (TextView) itemView.findViewById(R.id.text_message_body);
            timeText = (TextView) itemView.findViewById(R.id.text_message_time);
            nameText = (TextView) itemView.findViewById(R.id.text_message_name);
            profileImage = (ImageView) itemView.findViewById(R.id.image_message_profile);
        }

        void bind(Message message) {
            mItem = message;
            messageText.setText(message.getBody());

            // Format the stored timestamp into a readable String using method. EEE, dd MMM yyyy hh:mm
            SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm");
            timeText.setText(dateFormat.format(message.getSentDate()));
            nameText.setText(message.getReceiverUsername());

            // Insert the profile image from the URL into the ImageView.
            Picasso.with(mView.getContext()).load(message.getSenderProfilePicture()).into(profileImage);
        }
    }
}
