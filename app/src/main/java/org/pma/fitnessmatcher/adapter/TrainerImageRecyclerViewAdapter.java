package org.pma.fitnessmatcher.adapter;

import android.graphics.Color;
import android.graphics.Rect;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import org.pma.fitnessmatcher.R;
import org.pma.fitnessmatcher.TrainerProfileFragment.OnListFragmentInteractionListener;
import org.pma.fitnessmatcher.model.Document;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 *
 *
 */
public class TrainerImageRecyclerViewAdapter extends RecyclerView.Adapter<TrainerImageRecyclerViewAdapter.ViewHolder> {

    private final List<Document> mValues;
    private final List<Integer> delete;
    private final OnListFragmentInteractionListener mListener;

    public TrainerImageRecyclerViewAdapter(List<Document> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
        delete = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.trainer_profile_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        Document image = mValues.get(position);
        Picasso.with(holder.mView.getContext()).load(image.getImagePath()).into(holder.mContentView);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });

        holder.mContentView.setOnLongClickListener(new View.OnLongClickListener(){
            private Rect rect;

            @Override
            public boolean onLongClick(View v) {
                if (delete.contains(position)) {
                    holder.mContentView.setColorFilter(Color.argb(0, 0, 0, 0));
                    delete.remove(new Integer(position));
                }else {
                    holder.mContentView.setColorFilter(Color.argb(50, 0, 0, 0));
                    rect = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
                    delete.add(position);
                }
                return false;
            }
        });
    }

    public void addImage(Document trainerImage, int position) {
        mValues.add(position, trainerImage);
        notifyItemInserted(position);
    }
    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mContentView;
        public Document mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mContentView = view.findViewById(R.id.avatar);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mItem.getImagePath() + "'";
        }
    }
}
