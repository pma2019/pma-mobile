package org.pma.fitnessmatcher.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import org.pma.fitnessmatcher.R;
import org.pma.fitnessmatcher.TrainerTimeSlotsFragment;
import org.pma.fitnessmatcher.model.TimeSlot;

import java.util.List;

public class TrainerTimeSlotsRecyclerViewAdapter extends RecyclerView.Adapter<TrainerTimeSlotsRecyclerViewAdapter.ViewHolder> {
    private final List<TimeSlot> tValues;
    private final TrainerTimeSlotsFragment.OnTrainerTimeSlotsInteractionListener tListener;

    public TrainerTimeSlotsRecyclerViewAdapter(List<TimeSlot> tValues, TrainerTimeSlotsFragment.OnTrainerTimeSlotsInteractionListener mListener) {
        this.tValues = tValues;
        this.tListener = mListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.trainer_timeslots_list_item, parent, false);
        return new ViewHolder(view);
    }

    public void addTimeSlot(TimeSlot timeSlot) {
        tValues.add(timeSlot);
        notifyItemInserted(getItemCount() - 1);
    }

    @Override
    public void onBindViewHolder(final TrainerTimeSlotsRecyclerViewAdapter.ViewHolder holder, int position) {
        final TimeSlot timeSlot=tValues.get(position);
        ViewHolder viewHolder= holder;
        holder.bind(timeSlot);
        holder.tView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(null!=tListener) {
                    tListener.onTrainerTimeSlotsInteraction(timeSlot);
                }
            }
        });

        holder.tRemoveView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return tValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View tView;
        public final TextView terminView;
        public final ImageButton tRemoveView;
        public final TextView datumView;
        public final TextView vremeView;
        public TimeSlot tItem;

        public ViewHolder(View view) {
            super(view);
            tView = view;
            terminView = view.findViewById(R.id.termin);
            tRemoveView = view.findViewById(R.id.ukloni);
            datumView = view.findViewById(R.id.datum);
            vremeView = view.findViewById(R.id.vreme);
        }

        void bind(TimeSlot timeSlot){
            tItem=timeSlot;
            terminView.setText(timeSlot.getTrainerName());
            datumView.setText(timeSlot.getDay().toString()+"-"+timeSlot.getMonth().toString()+"-"+timeSlot.getYear().toString());
            vremeView.setText(timeSlot.getHour().toString()+":00");
            tRemoveView.setImageResource(R.drawable.ic_delete);
            //Picasso.with(tView.getContext()).load(R.drawable.ic_delete).into(tRemoveView);
//            Picasso.with(tView.getContext()).load(R.drawable.ic_delete_sth).into(tRemoveView);
         //   Picasso.with(tView.getContext()).load(R.drawable.ic_delete_sth).into(tRemoveView);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + tItem.getHour() + "'";
        }
    }
}
