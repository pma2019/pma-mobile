package org.pma.fitnessmatcher;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import org.pma.fitnessmatcher.adapter.TrainerTimeSlotsRecyclerViewAdapter;
import org.pma.fitnessmatcher.api.TimeSlotApi;
import org.pma.fitnessmatcher.model.TimeSlot;
import org.pma.fitnessmatcher.viewmodel.TimeSlotVM;
import org.pma.fitnessmatcher.viewmodel.TrainerTimeSlotsVM;
import org.pma.fitnessmatcher.viewmodel.ViewModelProviderFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import retrofit2.Response;
import retrofit2.Retrofit;

public class TrainerTimeSlotsFragment extends DaggerFragment {

    private static final String ARG_COLUMN_COUNT = "column-count";
    private int mColumnCount = 1;
    private TrainerTimeSlotsFragment.OnTrainerTimeSlotsInteractionListener mListener;
    private TrainerTimeSlotsVM viewModel;
    private TrainerTimeSlotsRecyclerViewAdapter adapter;

    @Inject
    protected UserSessionManager userSessionManager;

    @Inject
    ViewModelProviderFactory providerFactory;



    @Inject
    protected Retrofit retrofit;

    public TrainerTimeSlotsFragment() {
    }

    public static TrainerTimeSlotsFragment newInstance(int columnCount) {
        TrainerTimeSlotsFragment fragment = new TrainerTimeSlotsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof TrainerTimeSlotsFragment.OnTrainerTimeSlotsInteractionListener) {
            mListener = (TrainerTimeSlotsFragment.OnTrainerTimeSlotsInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnTrainerChooserInteractionListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.trainer_timeslots_list, container, false);
        RecyclerView recyclerView=view.findViewById(R.id.listTimeslots);

        Context context=recyclerView.getContext();
        if (mColumnCount <= 1) {
            recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        }else{
            recyclerView.setLayoutManager(new GridLayoutManager(recyclerView.getContext(), mColumnCount));
        }
        adapter = new TrainerTimeSlotsRecyclerViewAdapter(new ArrayList<TimeSlot>(), mListener);
        recyclerView.setAdapter(adapter);
        recyclerView.scrollToPosition(adapter.getItemCount() - 1);

        ImageButton addTimeSlot=view.findViewById(R.id.addButton);
        addTimeSlot.setOnClickListener(v -> {
            FragmentTransaction transaction=getFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment_container, AddTrainerTimeslotFragment.newInstance(1));
            transaction.addToBackStack(null);
            transaction.commit();
       });


        viewModel = ViewModelProviders.of(this, providerFactory).get(TrainerTimeSlotsVM.class);
        subscribe();
        return view;
    }

    private void subscribe() {
        viewModel.getAllByTrainer().observe(getViewLifecycleOwner(), timeSlots -> timeSlots.forEach(t -> adapter.addTimeSlot(t)));
        viewModel.getAllByTrainerStatus().observe(getViewLifecycleOwner(), status -> {
            Toast toast = Toast.makeText(getContext(), getResources().getString(R.string.time_slots_load_trainer_fail), getResources().getInteger(R.integer.duration));
            toast.setGravity(Gravity.TOP|Gravity.RIGHT, getResources().getInteger(R.integer.toast_xoffset), getResources().getInteger(R.integer.toast_yoffset));
            toast.show();
        });
    }


    @Override
    public void onDetach() {

        super.onDetach();
        mListener = null;
    }

    public interface OnTrainerTimeSlotsInteractionListener {
        void onTrainerTimeSlotsInteraction(TimeSlot item);
    }
}
