package org.pma.fitnessmatcher;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import androidx.lifecycle.ViewModelProviders;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.pma.fitnessmatcher.listeners.PictureChangeListener;
import org.pma.fitnessmatcher.model.UserProfile;
import org.pma.fitnessmatcher.viewmodel.PictureSettingsVM;
import org.pma.fitnessmatcher.viewmodel.ViewModelProviderFactory;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import lombok.Setter;

@Setter
public class ProfilePictureSettingsFragment extends DaggerFragment {
    private ImageView imageView;


    @Inject
    UserSessionManager userSessionManager;

    PictureSettingsVM viewModel;
    @Inject
    ViewModelProviderFactory providerFactory;

    private static final int REQUEST_IMAGE_CAPTURE = 1;

    private PictureChangeListener pictureChangeListener;
    public ProfilePictureSettingsFragment() {

    }
    public static ProfilePictureSettingsFragment newInstance(PictureChangeListener pictureChangeListener) {
        Bundle args = new Bundle();
        ProfilePictureSettingsFragment fragment = new ProfilePictureSettingsFragment();
        fragment.setPictureChangeListener(pictureChangeListener);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        UserProfile user = userSessionManager.getUserDetails();
        View rootView = inflater.inflate(R.layout.fragment_profile_picture, container, false);

        imageView = rootView.findViewById(R.id.profile_picture);
        Picasso.with(rootView.getContext()).load(user.getProfilePictureStorageUrl()).into(imageView);

        Button changePicture = rootView.findViewById(R.id.btnChange);
        changePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            }
        });
        viewModel = ViewModelProviders.of(this, providerFactory).get(PictureSettingsVM.class);
        subscribe();
        return rootView;
    }

    private void subscribe() {
        viewModel.getChangeAvatarStatus().observe(getViewLifecycleOwner(), status -> {
            if(status.equals(PictureSettingsVM.Status.ERROR)) {
                Toast toast = Toast.makeText(getContext(), getResources().getString(R.string.image_upload_failed), getResources().getInteger(R.integer.duration));
                toast.setGravity(Gravity.TOP | Gravity.RIGHT, getResources().getInteger(R.integer.toast_xoffset), getResources().getInteger(R.integer.toast_yoffset));
                toast.show();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");

            String path_external = Environment.getExternalStorageDirectory() + File.separator + "_avatar";
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            File imageFile = new File(path_external);
            try {
                imageFile.createNewFile();
                FileOutputStream fo = new FileOutputStream(imageFile);
                fo.write(bytes.toByteArray());
                fo.flush();
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            viewModel.changeAvatar(imageFile).observe(getViewLifecycleOwner(), document -> {
                if(document.getId() != null) {
                    UserProfile userProfile = userSessionManager.getUserDetails();
                    userProfile.setProfilePictureStorageUrl(document.getImagePath());
                    userSessionManager.setUserProfileDetails(userProfile);
                    Picasso.with(getContext()).load(userProfile.getProfilePictureStorageUrl()).into(imageView);
                    pictureChangeListener.onSuccess(userProfile.getProfilePictureStorageUrl());
                }
            });
        }
    }
}
