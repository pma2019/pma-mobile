package org.pma.fitnessmatcher;

import android.app.LoaderManager;
import android.content.Loader;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.pma.fitnessmatcher.api.TimeSlotApi;
import org.pma.fitnessmatcher.api.requests.AddTimeslotTrainerRequest;
import org.pma.fitnessmatcher.model.TimeSlot;

import java.io.IOException;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

public class AddTrainerTimeslotsActivity extends DaggerAppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private AddTrainerTimeslotTask task=null;

    private EditText mYearview;
    private EditText mMonthView;
    private EditText mDayView;
    private EditText mHourView;
    private Button mAddTs;

    private static final int REQUEST_PERMISSIONS = 0;

    @Inject
    UserSessionManager userSessionManager;

    @Inject
    protected Retrofit retrofit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_timeslot_trainer);

        mYearview=findViewById(R.id.addYear);
        mMonthView=findViewById(R.id.addMonth);
        mDayView=findViewById(R.id.addDay);
        mHourView=findViewById(R.id.addHour);
        mAddTs=(Button) findViewById(R.id.add_ts_trainer_button);

        /*Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });*/
        mAddTs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptAdd();
            }
        });
    }

    private void attemptAdd() {
        if (task != null) {
            return;
        }
     //   Integer year=mYearview.getText().;
        int year=Integer.parseInt(mYearview.getText().toString());
        int month=Integer.parseInt(mMonthView.getText().toString());
        int day=Integer.parseInt(mDayView.getText().toString());
        int hour=Integer.parseInt(mHourView.getText().toString());
        String email = userSessionManager.getUserDetails().getEmail();

        if(year!=0 && month!=0 && day!=0 && hour!=0 && !email.isEmpty() && email!=null){
            AddTrainerTimeslotTask task=new AddTrainerTimeslotTask((Integer) year,month,hour,day);
            task.execute((Void) null);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
    class AddTrainerTimeslotTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final Integer mYear;
        private final Integer mMonth;
        private final Integer mDay;
        private final Integer mHour;

        AddTrainerTimeslotTask(Integer mYear,Integer mMonth,Integer mDay,Integer mHour){
            mEmail= userSessionManager.getUserDetails().getEmail();
            this.mYear=mYear;
            this.mMonth=mMonth;
            this.mDay=mDay;
            this.mHour=mHour;
        }
        @Override
        protected Boolean doInBackground(Void... voids) {
            TimeSlotApi api=retrofit.create(TimeSlotApi.class);
            AddTimeslotTrainerRequest request=new AddTimeslotTrainerRequest(mEmail,mYear,mMonth,mDay,mHour);
            Call<TimeSlot> call=api.addTimeslotTrainer(request);
            try{
                Response<TimeSlot> response=call.execute();
                if(response.isSuccessful()){
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, TrainerTimeSlotsFragment.newInstance(1)).addToBackStack("trainer_timeslot").commit();
                    return true;
                }else{
                    Toast toast = Toast.makeText(getApplicationContext(), "Doslo je do grekse.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP|Gravity.RIGHT, getResources().getInteger(R.integer.toast_xoffset), getResources().getInteger(R.integer.toast_yoffset));
                    toast.show();
                    return false;
                }
            }catch (IOException e){
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(final Boolean success) {

        }
    }
}
