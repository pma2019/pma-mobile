package org.pma.fitnessmatcher;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.navigation.NavigationView;
import com.squareup.picasso.Picasso;

import org.pma.fitnessmatcher.listeners.PictureChangeListener;
import org.pma.fitnessmatcher.model.Message;
import org.pma.fitnessmatcher.model.TimeSlot;
import org.pma.fitnessmatcher.model.Document;
import org.pma.fitnessmatcher.model.TrainerProfile;
import org.pma.fitnessmatcher.model.UserProfile;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

public class MainActivity extends DaggerAppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        TimeSlotFragment.OnTraineeTimeSlotItemInteraction, TrainerProfileFragment.OnListFragmentInteractionListener,
        MessageFragment.OnListFragmentInteractionListener, InboxFragment.OnListInboxInteractionListener,
        TrainerChooserFragment.OnTrainerChooserInteractionListener, TrainerTimeSlotsFragment.OnTrainerTimeSlotsInteractionListener,
        AddTrainerTimeslotFragment.OnAddTrainerTimeSloInteractionListener{
    static final int REQUEST_IMAGE_CAPTURE = 1;
    private PictureChangeListener pictureChangeListener;

    @Inject
    protected UserSessionManager userSessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        UserProfile userProfile = userSessionManager.getUserDetails();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        TextView headerUserName = headerView.findViewById(R.id.header_username);
        headerUserName.setText(userProfile.getUsername());
        TextView headerEmail = headerView.findViewById(R.id.header_email);
        headerEmail.setText(userProfile.getEmail());
        ImageView avatar = headerView.findViewById(R.id.header_avatar);
        pictureChangeListener = new PictureChangeListener(avatar, navigationView.getContext());
        Picasso.with(navigationView.getContext()).load(userProfile.getProfilePictureStorageUrl()).into(avatar);
        navigationView.setNavigationItemSelectedListener(this);

        if(userSessionManager.isTrainer()){
            Menu menu = navigationView.getMenu();
            MenuItem gallery = menu.findItem(R.id.nav_gallery);
            gallery.setTitle(R.string.navigation_drawer_myprofile);

            MenuItem search = menu.findItem(R.id.nav_search);
            search.setVisible(false);

            getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, TrainerProfileFragment.newInstance(2,
                    userSessionManager.getUserDetails().getEmail(),userSessionManager.getUserDetails().getUsername(), true))
                    .addToBackStack("trainer-profile")
                    .commit();

        } else if(userSessionManager.isTrainee()) {
            Menu menu = navigationView.getMenu();
            MenuItem gallery = menu.findItem(R.id.nav_gallery);
            gallery.setVisible(false);
            getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, TrainerChooserFragment.newInstance(1))
                    .addToBackStack("trainer-chooser")
                    .commit();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_gallery) {
            if(userSessionManager.isTrainer()) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        TrainerProfileFragment
                                .newInstance(2,
                                        userSessionManager.getUserDetails().getEmail(),
                                        userSessionManager.getUserDetails().getUsername(),
                                        true))
                        .addToBackStack("gallery").commit();
            }
        } else if (id == R.id.nav_search) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, TrainerChooserFragment.newInstance(1))
                    .addToBackStack("trainer-chooser")
                    .commit();
        } else if (id == R.id.nav_manage) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, ProfileSettingsFragment.newInstance(pictureChangeListener)).addToBackStack("settings").commit();
        } else if(id == R.id.nav_calendar){
            if(userSessionManager.isTrainee()) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, TimeSlotFragment.newInstance(1)).addToBackStack("timeslot").commit();
            } else if(userSessionManager.isTrainer()){
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, TrainerTimeSlotsFragment.newInstance(1)).addToBackStack("trainer_timeslot").commit();
            }
        } else if(id == R.id.nav_messages){
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new InboxFragment()).addToBackStack("messages").commit();
        } else if(id == R.id.nav_signout) {
            userSessionManager.logout();
            FragmentManager fm = getSupportFragmentManager();
            for(int i = 0; i < fm.getBackStackEntryCount(); i++) {
                fm.popBackStack();
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onTrainerTimeSlotsInteraction(TimeSlot item) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, CommentFragment.newInstance(1, item.getId()))
                .addToBackStack("comment-list")
                .commit();
    }

    @Override
    public void onTraineeTimeSlotListInteraction(TimeSlot item) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, CommentFragment.newInstance(1, item.getId()))
                .addToBackStack("comment-list")
                .commit();
    }


    @Override
    public void onListFragmentInteraction(Document item) {

    }
    @Override
    public void onListFragmentInteraction(Message item) {

    }

    @Override
    public void onListInboxInteractionListener(Message item) {
        String email;
        String profilePicturePath;
        if(userSessionManager.getUserDetails().getEmail().equals(item.getReceiverEmail())) {
            email = item.getSenderEmail();
            profilePicturePath = item.getSenderProfilePicture();
        } else {
            email = item.getReceiverEmail();
            profilePicturePath = item.getReceiverProfilePicture();

        }
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, MessageFragment.newInstance(1, email, profilePicturePath))
                .addToBackStack("message_list")
                .commit();
    }

    @Override
    public void onTrainerChooserItemTouch(TrainerProfile item) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, TrainerProfileFragment.newInstance(2, item.getEmail(), item.getUsername(), false))
                .addToBackStack("trainer_chooser");
        fragmentTransaction.commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onAddTrainerTimeSlotInteraction(TimeSlot item) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, TrainerTimeSlotsFragment.newInstance(1))
                .addToBackStack("trainer_timeslot");
        fragmentTransaction.commit();
    }
}

