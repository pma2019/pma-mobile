package org.pma.fitnessmatcher.listeners;

import org.pma.fitnessmatcher.model.Comment;

public interface OnCommentListInteractionListener {
    void onCommentInteraction(Comment item);
}