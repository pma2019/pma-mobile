package org.pma.fitnessmatcher.listeners;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

import org.pma.fitnessmatcher.R;
import org.pma.fitnessmatcher.adapter.TimeSlotRecyclerViewAdapter;
import org.pma.fitnessmatcher.model.TimeSlot;

import java.util.List;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class TimeSlotListListener  {

    private TimeSlotRecyclerViewAdapter adapter;
    private Context context;
    public void onLoadList(List<TimeSlot> slots) {
        adapter.getMValues().clear();
        adapter.getMValues().addAll(slots);
        adapter.notifyDataSetChanged();
    }

    public void onAddOne(TimeSlot timeSlot) {
        adapter.getMValues().add(timeSlot);
        adapter.notifyItemInserted(adapter.getItemCount());
    }
    public void onError(String errorMessage) {
        int duration = context.getResources().getInteger(R.integer.duration);
        int xOffset = context.getResources().getInteger(R.integer.toast_xoffset);
        int yOffset = context.getResources().getInteger(R.integer.toast_yoffset);
        Toast toast = Toast.makeText(context, errorMessage, duration);
        toast.setGravity(Gravity.TOP, xOffset,yOffset);
        toast.show();
    }
}
