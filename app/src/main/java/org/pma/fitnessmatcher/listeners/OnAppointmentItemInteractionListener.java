package org.pma.fitnessmatcher.listeners;

import org.pma.fitnessmatcher.model.TimeSlot;

public interface OnAppointmentItemInteractionListener {
    void onListFragmentInteraction(TimeSlot item);
}