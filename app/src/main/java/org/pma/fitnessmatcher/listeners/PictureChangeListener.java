package org.pma.fitnessmatcher.listeners;

import android.content.Context;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class PictureChangeListener {
    private ImageView imageView;
    private Context context;

    public void onSuccess(String imagePath) {
        Picasso.with(context).load(imagePath).into(imageView);
    }
}
