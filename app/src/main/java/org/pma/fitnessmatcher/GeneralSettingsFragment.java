package org.pma.fitnessmatcher;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import org.pma.fitnessmatcher.model.UserProfile;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import lombok.Setter;
import retrofit2.Retrofit;

@Setter
public class GeneralSettingsFragment extends DaggerFragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    @Inject
    protected UserSessionManager userSessionManager;

    public GeneralSettingsFragment() {
    }

    public static GeneralSettingsFragment newInstance() {
        Bundle args = new Bundle();
        GeneralSettingsFragment fragment = new GeneralSettingsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile_general, container, false);
        UserProfile user = userSessionManager.getUserDetails();
        EditText firstName = rootView.findViewById(R.id.first_name);
        firstName.setText(user.getName(), TextView.BufferType.EDITABLE);
        firstName.setFocusable(false);

        EditText lastName = rootView.findViewById(R.id.last_name);
        lastName.setText(user.getLastName(), TextView.BufferType.EDITABLE);
        lastName.setFocusable(false);

        EditText email = rootView.findViewById(R.id.email_general_setting);
        email.setText(user.getEmail(), TextView.BufferType.EDITABLE);
        email.setFocusable(false);


        return rootView;
    }
}