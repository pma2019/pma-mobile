package org.pma.fitnessmatcher.module.builder;

import org.pma.fitnessmatcher.LoginActivity;
import org.pma.fitnessmatcher.MainActivity;
import org.pma.fitnessmatcher.RegisterActivity;
import org.pma.fitnessmatcher.module.LoginModule;
import org.pma.fitnessmatcher.module.builder.MainActivityBuildersModule;
import org.pma.fitnessmatcher.module.viewmodel.LoginVMModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuildersModule {

    @ContributesAndroidInjector(
            modules = {LoginVMModule.class, LoginModule.class})
    abstract LoginActivity contributeAuthActivity();


    @ContributesAndroidInjector(
            modules = {MainActivityBuildersModule.class}
    )
    abstract MainActivity contributeMainActivity();

    @ContributesAndroidInjector()
    abstract RegisterActivity contributeRegisterActivity();
}
