package org.pma.fitnessmatcher.module.viewmodel;

import androidx.lifecycle.ViewModel;

import org.pma.fitnessmatcher.viewmodel.LoginVM;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class LoginVMModule {

    @Binds
    @IntoMap
    @ViewModelKey(LoginVM.class)
    public abstract ViewModel bind(LoginVM viewModel);
}
