package org.pma.fitnessmatcher.module;

import org.pma.fitnessmatcher.UserSessionManager;
import org.pma.fitnessmatcher.api.CommentApi;
import org.pma.fitnessmatcher.repository.CommentRepository;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class CommentModule {

    @Provides
    static CommentApi provideCommentApi(Retrofit retrofit) {
        return retrofit.create(CommentApi.class);
    }
    @Provides
    static CommentRepository provideCommentRepository(CommentApi commentApi, UserSessionManager userSessionManager) {
        return new CommentRepository(commentApi, userSessionManager);
    }
}
