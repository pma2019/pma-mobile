package org.pma.fitnessmatcher.module.viewmodel;

import androidx.lifecycle.ViewModel;

import org.pma.fitnessmatcher.CommentFragment;
import org.pma.fitnessmatcher.viewmodel.CommentVM;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;

@Module
public abstract class CommentVMModule {

    @Provides
    static Integer getTimeSlotId(CommentFragment fragment) {
        return fragment.getArguments().getInt(CommentFragment.TIME_SLOT_ID);
    }

    @Binds
    @IntoMap
    @ViewModelKey(CommentVM.class)
    public abstract ViewModel bindMessage(CommentVM viewModel);

}
