package org.pma.fitnessmatcher.module.viewmodel;

import androidx.lifecycle.ViewModel;

import org.pma.fitnessmatcher.viewmodel.MessageVM;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class MessageVMModule {
    @Binds
    @IntoMap
    @ViewModelKey(MessageVM.class)
    public abstract ViewModel bindMessage(MessageVM viewModel);
}
