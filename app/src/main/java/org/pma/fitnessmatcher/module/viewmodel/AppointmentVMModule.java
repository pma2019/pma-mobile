package org.pma.fitnessmatcher.module.viewmodel;

import androidx.lifecycle.ViewModel;

import org.pma.fitnessmatcher.AppointmentFragment;
import org.pma.fitnessmatcher.viewmodel.AppointmentVM;
import org.pma.fitnessmatcher.viewmodel.MessageVM;

import javax.inject.Named;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;

@Module
public abstract class AppointmentVMModule extends ViewModel {

    @Provides
    @Named("trainerEmail")
    static String getTrainerEmail(AppointmentFragment fragment){
        return fragment.getArguments().getString(AppointmentFragment.ARG_TRAINER_EMAIL);
    }

    @Binds
    @IntoMap
    @ViewModelKey(AppointmentVM.class)
    public abstract ViewModel bindMessage(AppointmentVM viewModel);
}
