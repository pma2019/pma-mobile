package org.pma.fitnessmatcher.module;

import org.pma.fitnessmatcher.api.UserApi;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class LoginModule {

    @Provides
    static UserApi provideSessionApi(Retrofit retrofit){
        return retrofit.create(UserApi.class);
    }

}
