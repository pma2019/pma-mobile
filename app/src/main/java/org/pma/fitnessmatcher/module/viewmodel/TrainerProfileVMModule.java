package org.pma.fitnessmatcher.module.viewmodel;

import androidx.lifecycle.ViewModel;

import org.pma.fitnessmatcher.TrainerProfileFragment;
import org.pma.fitnessmatcher.viewmodel.TrainerProfileVM;

import javax.inject.Named;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;

@Module
public abstract class TrainerProfileVMModule {

    @Provides
    @Named("trainerProfileEmail")
    static String trainerProfileEmail(TrainerProfileFragment fragment) {
        return fragment.getArguments().getString(TrainerProfileFragment.ARG_TRAINER_EMAIL);
    }

    @Binds
    @IntoMap
    @ViewModelKey(TrainerProfileVM.class)
    public abstract ViewModel bind(TrainerProfileVM viewModel);
}
