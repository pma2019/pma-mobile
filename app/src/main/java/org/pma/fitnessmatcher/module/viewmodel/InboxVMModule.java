package org.pma.fitnessmatcher.module.viewmodel;

import androidx.lifecycle.ViewModel;

import org.pma.fitnessmatcher.viewmodel.InboxVM;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class InboxVMModule {

    @Binds
    @IntoMap
    @ViewModelKey(InboxVM.class)
    public abstract ViewModel bind(InboxVM viewModel);
}
