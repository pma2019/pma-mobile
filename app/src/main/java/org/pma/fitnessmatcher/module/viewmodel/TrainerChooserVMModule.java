package org.pma.fitnessmatcher.module.viewmodel;

import androidx.lifecycle.ViewModel;

import org.pma.fitnessmatcher.viewmodel.TrainerChooserVM;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class TrainerChooserVMModule {
    @Binds
    @IntoMap
    @ViewModelKey(TrainerChooserVM.class)
    public abstract ViewModel bind(TrainerChooserVM viewModel);
}
