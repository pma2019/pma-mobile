package org.pma.fitnessmatcher.module;

import org.pma.fitnessmatcher.UserSessionManager;
import org.pma.fitnessmatcher.api.TimeSlotApi;
import org.pma.fitnessmatcher.database.dao.TimeSlotDao;
import org.pma.fitnessmatcher.repository.TimeSlotRepository;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class TimeSlotModule {
    @Provides
    static TimeSlotApi provideTimeSlotApi(Retrofit retrofit) {
        return retrofit.create(TimeSlotApi.class);
    }

    @Provides
    static TimeSlotRepository provideTimeSlotRepository(TimeSlotApi timeSlotApi, UserSessionManager userSessionManager) {
        return new TimeSlotRepository(timeSlotApi, userSessionManager);
    }

}
