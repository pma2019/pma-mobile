package org.pma.fitnessmatcher.module;

import org.pma.fitnessmatcher.UserSessionManager;
import org.pma.fitnessmatcher.api.RatingApi;
import org.pma.fitnessmatcher.repository.RatingsRepository;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class RatingModule {

    @Provides
    static RatingApi provideRatingApi(Retrofit retrofit) {
        return retrofit.create(RatingApi.class);
    }

    @Provides
    static RatingsRepository provideRatingsRepository(RatingApi ratingApi, UserSessionManager userSessionManager) {
        return new RatingsRepository(ratingApi, userSessionManager);
    }
}

