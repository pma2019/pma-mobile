package org.pma.fitnessmatcher.module.builder;

import org.pma.fitnessmatcher.GeneralSettingsFragment;
import org.pma.fitnessmatcher.PrivacySettingsFragment;
import org.pma.fitnessmatcher.ProfilePictureSettingsFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;


@Module
public abstract class SettingsFragmentBuilderModule {

    @ContributesAndroidInjector()
    abstract GeneralSettingsFragment contributeGeneralSettingsFragment();

    @ContributesAndroidInjector()
    abstract PrivacySettingsFragment contributePrivacySettingsFragment();

    @ContributesAndroidInjector()
    abstract ProfilePictureSettingsFragment contributeProfilePictureSettingsFragment();


}
