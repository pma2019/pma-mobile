package org.pma.fitnessmatcher.module;

import androidx.lifecycle.ViewModelProvider;

import org.pma.fitnessmatcher.viewmodel.ViewModelProviderFactory;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module
public abstract class ViewModelFactoryModule {

    @Binds
    public abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelProviderFactory viewModelFactory);
}