package org.pma.fitnessmatcher.module.viewmodel;

import androidx.lifecycle.ViewModel;

import org.pma.fitnessmatcher.viewmodel.GeneralSettingsVM;
import org.pma.fitnessmatcher.viewmodel.PrivacySettingsVM;
import org.pma.fitnessmatcher.viewmodel.PictureSettingsVM;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class SettingsVMModule {
    @Binds
    @IntoMap
    @ViewModelKey(PrivacySettingsVM.class)
    public abstract ViewModel bindPrivacy(PrivacySettingsVM viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PictureSettingsVM.class)
    public abstract ViewModel bindPicture(PictureSettingsVM viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(GeneralSettingsVM.class)
    public abstract ViewModel bindGeneral(GeneralSettingsVM viewModel);




}
