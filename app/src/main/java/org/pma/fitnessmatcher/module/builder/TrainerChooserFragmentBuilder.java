package org.pma.fitnessmatcher.module.builder;

import org.pma.fitnessmatcher.AddTrainerTimeslotFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class TrainerChooserFragmentBuilder {
    @ContributesAndroidInjector
    abstract AddTrainerTimeslotFragment contributeAddTrainerTimeslotFragment();
}
