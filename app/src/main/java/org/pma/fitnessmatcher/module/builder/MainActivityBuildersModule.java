package org.pma.fitnessmatcher.module.builder;

import org.pma.fitnessmatcher.AppointmentFragment;
import org.pma.fitnessmatcher.CommentFragment;
import org.pma.fitnessmatcher.InboxFragment;
import org.pma.fitnessmatcher.MessageFragment;
import org.pma.fitnessmatcher.ProfileSettingsFragment;
import org.pma.fitnessmatcher.TimeSlotFragment;
import org.pma.fitnessmatcher.TrainerChooserFragment;
import org.pma.fitnessmatcher.TrainerProfileFragment;
import org.pma.fitnessmatcher.TrainerTimeSlotsFragment;
import org.pma.fitnessmatcher.module.CommentModule;
import org.pma.fitnessmatcher.module.DocumentModule;
import org.pma.fitnessmatcher.module.InboxModule;
import org.pma.fitnessmatcher.module.RatingModule;
import org.pma.fitnessmatcher.module.TimeSlotModule;
import org.pma.fitnessmatcher.module.UserModule;
import org.pma.fitnessmatcher.module.viewmodel.AppointmentVMModule;
import org.pma.fitnessmatcher.module.viewmodel.CommentVMModule;
import org.pma.fitnessmatcher.module.viewmodel.InboxVMModule;
import org.pma.fitnessmatcher.module.MessageModule;
import org.pma.fitnessmatcher.module.viewmodel.MessageVMModule;
import org.pma.fitnessmatcher.module.viewmodel.SettingsVMModule;
import org.pma.fitnessmatcher.module.viewmodel.TimeSlotVMModule;
import org.pma.fitnessmatcher.module.viewmodel.TrainerChooserVMModule;
import org.pma.fitnessmatcher.module.viewmodel.TrainerProfileVMModule;
import org.pma.fitnessmatcher.module.viewmodel.TrainerTimeSlotsVMModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MainActivityBuildersModule {

    @ContributesAndroidInjector(modules = {SettingsFragmentBuilderModule.class, SettingsVMModule.class, UserModule.class, DocumentModule.class})
    abstract ProfileSettingsFragment contributeGeneralSettingsFragment();

    @ContributesAndroidInjector(modules = {InboxVMModule.class, InboxModule.class})
    abstract InboxFragment contributeInboxFragment();

    @ContributesAndroidInjector(modules = {MessageVMModule.class, InboxModule.class, MessageModule.class})
    abstract MessageFragment contributeMessageFragment();

    @ContributesAndroidInjector(modules = {TimeSlotVMModule.class, TimeSlotModule.class})
    abstract TimeSlotFragment contributeTimeSlotFragment();

    @ContributesAndroidInjector(modules = {TrainerTimeSlotsVMModule.class, TimeSlotModule.class})
    abstract TrainerTimeSlotsFragment contributeTrainerTimeSlotsFragment();

    @ContributesAndroidInjector(modules = {CommentVMModule.class, CommentModule.class, RatingModule.class})
    abstract CommentFragment contributeCommentFragment();

    @ContributesAndroidInjector(modules = {AppointmentVMModule.class, TimeSlotModule.class})
    abstract AppointmentFragment contributeAppointmentFragment();

    @ContributesAndroidInjector(modules = {TrainerChooserFragmentBuilder.class, TrainerChooserVMModule.class, UserModule.class})
    abstract TrainerChooserFragment contributeTrainerChooserFragment();

    @ContributesAndroidInjector(modules = {TrainerProfileVMModule.class, UserModule.class, DocumentModule.class})
    abstract TrainerProfileFragment contributeTrainerProfileFragment();



}
