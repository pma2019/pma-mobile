package org.pma.fitnessmatcher.module;

import org.pma.fitnessmatcher.UserSessionManager;
import org.pma.fitnessmatcher.api.DocumentApi;
import org.pma.fitnessmatcher.repository.DocumentRepository;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class DocumentModule {
    @Provides
    static DocumentApi provideDocumentApi(Retrofit retrofit) {
        return retrofit.create(DocumentApi.class);
    }

    @Provides
    static DocumentRepository provideDocumentRepository(DocumentApi documentApi, UserSessionManager userSessionManager) {
        return new DocumentRepository(documentApi, userSessionManager);
    }
}
