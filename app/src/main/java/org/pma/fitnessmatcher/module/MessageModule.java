package org.pma.fitnessmatcher.module;

import org.pma.fitnessmatcher.MessageFragment;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public abstract class MessageModule {

    @Provides
    @Named("fromEmail")
    static String getEmail(MessageFragment fragment) {
        return fragment.getArguments().getString(MessageFragment.CHATTER_EMAIL);
    }


}
