package org.pma.fitnessmatcher.module;

import org.pma.fitnessmatcher.api.DocumentApi;
import org.pma.fitnessmatcher.api.UserApi;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class SettingsModule {

    @Provides
    static UserApi provideUserApi(Retrofit retrofit) {
        return retrofit.create(UserApi.class);
    }

    @Provides
    static DocumentApi provideDocumentApi(Retrofit retrofit) {
        return retrofit.create(DocumentApi.class);
    }
}
