package org.pma.fitnessmatcher.module.viewmodel;

import androidx.lifecycle.ViewModel;

import org.pma.fitnessmatcher.viewmodel.TimeSlotVM;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class TimeSlotVMModule {

    @Binds
    @IntoMap
    @ViewModelKey(TimeSlotVM.class)
    public abstract ViewModel bind(TimeSlotVM viewModel);
}
