package org.pma.fitnessmatcher.module;

import com.google.gson.Gson;

import org.pma.fitnessmatcher.UserSessionManager;
import org.pma.fitnessmatcher.api.MessagesApi;
import org.pma.fitnessmatcher.repository.MessageRepository;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import ua.naiksoftware.stomp.Stomp;
import ua.naiksoftware.stomp.StompClient;

@Module
public abstract class InboxModule {

    @Provides
    static MessagesApi messagesApi(Retrofit retrofit) {
        return retrofit.create(MessagesApi.class);
    }

    @Provides
    static StompClient provideStompClient() {
        return Stomp.over(Stomp.ConnectionProvider.OKHTTP, "ws://192.168.0.11:8080/core/chat");
    }

    @Provides
    static MessageRepository messageRepository(MessagesApi messagesApi, UserSessionManager userSessionManager, StompClient stompClient, Gson gson) {
        return new MessageRepository(messagesApi, userSessionManager, stompClient, gson);
    }

}
