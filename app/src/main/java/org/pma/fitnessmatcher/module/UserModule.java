package org.pma.fitnessmatcher.module;

import org.pma.fitnessmatcher.UserSessionManager;
import org.pma.fitnessmatcher.api.UserApi;
import org.pma.fitnessmatcher.database.dao.TrainerDao;
import org.pma.fitnessmatcher.repository.UserRepository;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class UserModule {

    @Provides
    static UserApi provideUserApi(Retrofit retrofit) {
        return retrofit.create(UserApi.class);
    }

    @Provides
    static UserRepository provideUserRepository(UserSessionManager userSessionManager, TrainerDao trainerDao, UserApi userApi) {
        return new UserRepository(userApi, trainerDao, userSessionManager);
    }
}
