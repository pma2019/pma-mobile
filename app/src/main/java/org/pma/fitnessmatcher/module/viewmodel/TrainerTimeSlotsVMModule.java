package org.pma.fitnessmatcher.module.viewmodel;

import androidx.lifecycle.ViewModel;

import org.pma.fitnessmatcher.viewmodel.TrainerTimeSlotsVM;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class TrainerTimeSlotsVMModule {
    @Binds
    @IntoMap
    @ViewModelKey(TrainerTimeSlotsVM.class)
    public abstract ViewModel bind(TrainerTimeSlotsVM viewModel);
}
