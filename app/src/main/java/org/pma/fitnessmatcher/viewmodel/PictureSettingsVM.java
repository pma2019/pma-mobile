package org.pma.fitnessmatcher.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.LiveDataReactiveStreams;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import org.pma.fitnessmatcher.model.Document;
import org.pma.fitnessmatcher.repository.DocumentRepository;

import java.io.File;

import javax.inject.Inject;

import io.reactivex.Flowable;
import lombok.Getter;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.HttpException;

public class PictureSettingsVM extends ViewModel {
    private DocumentRepository documentRepository;
    @Getter
    MutableLiveData<Status> changeAvatarStatus;

    public enum Status {
        ERROR,
        SUCCESS,
        NOT_FOUND
    }
    @Inject
    public PictureSettingsVM(DocumentRepository documentRepository) {
        this.documentRepository = documentRepository;
        this.changeAvatarStatus = new MutableLiveData<>();
    }


    public LiveData<Document> changeAvatar(File imageFile) {
        MultipartBody.Part filePart = MultipartBody.Part.createFormData("image", imageFile.getName(), RequestBody.create(MediaType.parse("image/*"), imageFile));
        MultipartBody.Part emailPart = MultipartBody.Part.createFormData("email", documentRepository.getUserSessionManager().getUserDetails().getEmail());
        MultipartBody.Part avatarPart = MultipartBody.Part.createFormData("avatar", Boolean.toString(true));
        return LiveDataReactiveStreams.fromPublisher(documentRepository.changeAvatar(filePart, emailPart, avatarPart).onErrorReturn(throwable -> {
            changeAvatarStatus.postValue(Status.ERROR);
            return new Document();
        }));
    }

}
