package org.pma.fitnessmatcher.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.LiveDataReactiveStreams;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import org.pma.fitnessmatcher.UserSessionManager;
import org.pma.fitnessmatcher.api.TimeSlotApi;
import org.pma.fitnessmatcher.model.TimeSlot;
import org.pma.fitnessmatcher.repository.TimeSlotRepository;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import lombok.Getter;

public class TrainerTimeSlotsVM extends ViewModel {

    private TimeSlotRepository repository;
    @Getter
    private MutableLiveData<Status> allByTrainerStatus;

    @Getter
    MediatorLiveData<List<TimeSlot>> allByTrainer;

    public enum Status {
        SUCCESS,
        ERROR,
        NOT_FOUND
    }

    @Inject
    public TrainerTimeSlotsVM(TimeSlotRepository repository) {
        this.repository = repository;
        this.allByTrainerStatus = new MutableLiveData<>();
        LiveData<List<TimeSlot>> allByTrainerLiveData = allByTrainer();
        this.allByTrainer = new MediatorLiveData<>();
        this.allByTrainer.addSource(allByTrainerLiveData, timeSlots -> {
            if(!timeSlots.isEmpty()) {
                this.allByTrainer.removeSource(allByTrainerLiveData);
                this.allByTrainer.setValue(timeSlots);
            }
        });
    }

    private LiveData<List<TimeSlot>> allByTrainer() {
        return LiveDataReactiveStreams.fromPublisher(repository.getAllByTrainer(this.repository.getUserSessionManager().getUserDetails().getEmail())
                .onErrorReturn(throwable -> {
                this.allByTrainerStatus.postValue(TrainerTimeSlotsVM.Status.ERROR);
                return new ArrayList<>();
        }));
    }


}
