package org.pma.fitnessmatcher.viewmodel;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.LiveDataReactiveStreams;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import org.pma.fitnessmatcher.UserSessionManager;
import org.pma.fitnessmatcher.api.TimeSlotApi;
import org.pma.fitnessmatcher.api.requests.MakeAppointmentRequest;
import org.pma.fitnessmatcher.model.Resource;
import org.pma.fitnessmatcher.model.TimeSlot;
import org.pma.fitnessmatcher.repository.TimeSlotRepository;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Flowable;
import lombok.Getter;
import lombok.Setter;

// https://stackoverflow.com/questions/44208618/how-to-handle-error-states-with-livedata
// https://android.jlelse.eu/android-mvvm-with-dagger-2-retrofit-rxjava-architecture-components-6f5da1a75135
public class AppointmentVM extends ViewModel {
    private TimeSlotRepository repository;

    @Getter
    private MutableLiveData<Status> makeAppoinStatus;

    @Getter
    private MutableLiveData<Status> allByTrainerStatus;

    @Getter
    private MediatorLiveData<List<TimeSlot>> allByTrainer;

    String trainerEmail;

    public enum Status {
        SUCCESS,
        ERROR,
        NOT_FOUND
    }
    @Inject
    public AppointmentVM(TimeSlotRepository repository, @Named("trainerEmail") String trainerEmail) {
        this.repository = repository;
        makeAppoinStatus = new MutableLiveData<>();
        allByTrainerStatus = new MutableLiveData<>();
        this.trainerEmail = trainerEmail;
        LiveData<List<TimeSlot>> allByTrainerLiveData = allByTrainer();
        this.allByTrainer = new MediatorLiveData<>();
        this.allByTrainer.addSource(allByTrainerLiveData, timeSlots -> {
            if(!timeSlots.isEmpty()) {
                this.allByTrainer.removeSource(allByTrainerLiveData);
                this.allByTrainer.setValue(timeSlots);
            }
        });
    }

    private LiveData<List<TimeSlot>> allByTrainer() {
        return LiveDataReactiveStreams.fromPublisher(repository.getAllByTrainer(trainerEmail).onErrorReturn(throwable -> {
            allByTrainerStatus.postValue(Status.ERROR);
            return new ArrayList<>();
        }));
    }

    public LiveData<TimeSlot> makeAppointment(Integer id) {
        MakeAppointmentRequest request = new MakeAppointmentRequest();
        request.setTimeSlotId(id);
        request.setEmail(repository.getUserSessionManager().getUserDetails().getEmail());
        return LiveDataReactiveStreams.fromPublisher(repository.addTimeslotTrainee(request).onErrorReturn(throwable -> {
            makeAppoinStatus.postValue(Status.ERROR);
            return new TimeSlot();
        }));
    }

}
