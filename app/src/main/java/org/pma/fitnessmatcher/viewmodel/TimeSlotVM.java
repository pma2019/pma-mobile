package org.pma.fitnessmatcher.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.LiveDataReactiveStreams;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import org.pma.fitnessmatcher.model.TimeSlot;
import org.pma.fitnessmatcher.repository.TimeSlotRepository;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import lombok.Getter;

public class TimeSlotVM extends ViewModel {
    private TimeSlotRepository repository;
    @Getter
    private MutableLiveData<Status> allByTraineeStatus;

    @Getter
    MediatorLiveData<List<TimeSlot>> allByTrainee;

    public enum Status {
        SUCCESS,
        ERROR,
        NOT_FOUND
    }

    @Inject
    public TimeSlotVM(TimeSlotRepository repository) {
        this.repository = repository;
        this.allByTraineeStatus = new MutableLiveData<>();
        LiveData<List<TimeSlot>> allByTraineeLiveData = allByTrainee();
        allByTrainee = new MediatorLiveData<>();
        allByTrainee.addSource(allByTraineeLiveData, timeSlots -> {
            if(!timeSlots.isEmpty()) {
                allByTrainee.removeSource(allByTraineeLiveData);
                allByTrainee.setValue(timeSlots);
            }
        });
    }

    private LiveData<List<TimeSlot>> allByTrainee() {
        return LiveDataReactiveStreams.fromPublisher(repository.getAllByTrainee().onErrorReturn(throwable -> {
            allByTraineeStatus.postValue(Status.ERROR);
            return new ArrayList<>();
        }));
    }



}
