package org.pma.fitnessmatcher.viewmodel;

import androidx.lifecycle.ViewModel;

import org.pma.fitnessmatcher.UserSessionManager;
import org.pma.fitnessmatcher.api.UserApi;

import javax.inject.Inject;

public class GeneralSettingsVM extends ViewModel {
    private UserSessionManager userSessionManager;
    private UserApi userApi;

    @Inject
    public GeneralSettingsVM(UserSessionManager userSessionManager, UserApi userApi) {
        this.userSessionManager = userSessionManager;
        this.userApi = userApi;
    }
}
