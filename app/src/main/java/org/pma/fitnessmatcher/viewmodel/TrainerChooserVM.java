package org.pma.fitnessmatcher.viewmodel;

import android.annotation.SuppressLint;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import org.pma.fitnessmatcher.model.TrainerProfile;
import org.pma.fitnessmatcher.repository.UserRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import lombok.Getter;

public class TrainerChooserVM extends ViewModel {
    UserRepository userRepository;
    @Getter
    MutableLiveData<Status> status;

    @Getter
    MediatorLiveData<List<TrainerProfile>> allTrainers;

    LiveData<List<TrainerProfile>> allDB;

    private final String GET_ALL = this.getClass().getSimpleName() + "_" + "GET_ALL";

    public enum Status {
        ERROR,
        SUCCESS,
        NOT_FOUND
    }
    @Inject
    public TrainerChooserVM(UserRepository userRepository) {
        this.userRepository = userRepository;
        this.status = new MutableLiveData<>();
        this.allTrainers = new MediatorLiveData<>();
        allDB = this.userRepository.getAllTrainerProfileDB();
        getAll();
    }

    @SuppressLint("CheckResult")
    private MediatorLiveData<List<TrainerProfile>> getAll() {
        allTrainers.addSource(allDB, trainerProfiles -> {
            if((trainerProfiles == null || trainerProfiles.isEmpty()) || this.userRepository.doRefresh(GET_ALL)) {
                this.userRepository.getTrainers().onErrorReturn(throwable -> {
                    status.postValue(Status.ERROR);
                    return new ArrayList<>();
                }).subscribe(list -> {
                    list.forEach(t -> userRepository.insert(t));
                    this.userRepository.updateRefresh(GET_ALL, new Date());
                });
            } else {
                allTrainers.removeSource(allDB);
                allTrainers.setValue(trainerProfiles);
            }
        });
        return allTrainers;
    }


}
