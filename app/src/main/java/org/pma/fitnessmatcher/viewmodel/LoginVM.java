package org.pma.fitnessmatcher.viewmodel;

import androidx.lifecycle.ViewModel;

import org.pma.fitnessmatcher.UserSessionManager;
import org.pma.fitnessmatcher.api.UserApi;

import javax.inject.Inject;

public class LoginVM extends ViewModel {

    private final UserApi userApi;
    private UserSessionManager userSessionManager;

    @Inject
    public LoginVM(UserApi userApi, UserSessionManager userSessionManager) {
        this.userApi = userApi;
        this.userSessionManager = userSessionManager;
    }


}
