package org.pma.fitnessmatcher.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.LiveDataReactiveStreams;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import org.pma.fitnessmatcher.api.requests.AddCommentRequest;
import org.pma.fitnessmatcher.api.requests.AddRatingRequest;
import org.pma.fitnessmatcher.api.responses.OverAllRating;
import org.pma.fitnessmatcher.model.Comment;
import org.pma.fitnessmatcher.model.Ratings;
import org.pma.fitnessmatcher.repository.CommentRepository;
import org.pma.fitnessmatcher.repository.RatingsRepository;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import lombok.Getter;
import retrofit2.HttpException;

public class CommentVM extends ViewModel {
    private RatingsRepository ratingsRepository;
    private CommentRepository commentRepository;

    @Getter
    private MutableLiveData<Status> statusByTrainee;

    @Getter
    private MutableLiveData<Status> statusByTrainer;

    @Getter
    LiveData<Ratings> ratingByTrainee;

    @Getter
    LiveData<OverAllRating> ratingByTrainer;

    @Getter
    private MutableLiveData<Status> addCommentStatus;

    @Getter
    private MutableLiveData<Status> addRatingStatus;

    @Getter
    private MutableLiveData<Status> allCommentsByTimeSlotStatus;

    @Getter
    LiveData<List<Comment>> allCommentsByTimeSlot;

    private Integer timeSlotId;

    public enum Status {
        SUCCESS,
        ERROR,
        NOT_FOUND
    }

    @Inject
    public CommentVM(RatingsRepository ratingsRepository, CommentRepository commentRepository, Integer timeSlotId) {
       this.timeSlotId = timeSlotId;
       this.ratingsRepository = ratingsRepository;
       this.commentRepository = commentRepository;
       statusByTrainee = new MutableLiveData<>();
       statusByTrainer = new MutableLiveData<>();
       allCommentsByTimeSlotStatus = new MutableLiveData<>();
       addCommentStatus = new MutableLiveData<>();
       addRatingStatus = new MutableLiveData<>();
       ratingByTrainee = ratingByTrainee(timeSlotId);
       ratingByTrainer = ratingByTrainer(timeSlotId);
       allCommentsByTimeSlot = allCommentsByTimeSlot(timeSlotId);

    }


    private LiveData<Ratings> ratingByTrainee(Integer id) {
        return LiveDataReactiveStreams.fromPublisher(ratingsRepository.getByTraineeAndTimeSlot(id).onErrorReturn(e -> {
           if(((HttpException) e).code() == 404) {
               statusByTrainee.postValue(Status.NOT_FOUND);
           } else if(((HttpException) e).code() > 404) {
               statusByTrainee.postValue(Status.ERROR);
           }
           return new Ratings();
        }));
    }

    private LiveData<OverAllRating> ratingByTrainer(Integer id) {
        return LiveDataReactiveStreams.fromPublisher(ratingsRepository.getByTrainerAndTimeSlot(id).onErrorReturn(e -> {
            if(((HttpException) e).code() >= 300) {
                statusByTrainer.postValue(Status.ERROR);
            }
            return new OverAllRating();
        }));
    }

    private LiveData<List<Comment>> allCommentsByTimeSlot(Integer id) {
        return LiveDataReactiveStreams.fromPublisher(commentRepository.getAllByTimeSlot(id).onErrorReturn(e -> {
            if(((HttpException) e).code() >= 300) {
                allCommentsByTimeSlotStatus.postValue(Status.ERROR);
            }
            return new ArrayList<>();
        }));
    }

    public LiveData<Comment> addComment(boolean byTrainer, String body, Integer timeSlot, String email) {
        AddCommentRequest request = new AddCommentRequest();
        request.setByTrainer(byTrainer);
        request.setBody(body);
        request.setCreateByEmail(email);
        request.setTimeSlotId(timeSlot);
        return LiveDataReactiveStreams.fromPublisher(commentRepository.addComment(request).onErrorReturn(throwable -> {
            addRatingStatus.postValue(Status.ERROR);
            return new Comment();
        }));
    }

    public LiveData<Ratings> addRatings(Float ratingNumber, Integer id, String email) {
        AddRatingRequest rating = new AddRatingRequest();
        rating.setRatingNumber(ratingNumber);
        rating.setTimeSlotId(id);
        rating.setTraineeEmail(email);
        return LiveDataReactiveStreams.fromPublisher(ratingsRepository.addRating(rating).onErrorReturn(throwable -> {
            addRatingStatus.postValue(Status.ERROR);
            return new Ratings();
        }));
    }





}
