package org.pma.fitnessmatcher.viewmodel;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.LiveDataReactiveStreams;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import org.pma.fitnessmatcher.api.requests.AddMessageRequest;
import org.pma.fitnessmatcher.model.Message;
import org.pma.fitnessmatcher.repository.MessageRepository;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import lombok.Getter;
import retrofit2.HttpException;

public class MessageVM extends ViewModel {
    MessageRepository messageRepository;

    @Getter
    MediatorLiveData<List<Message>> fromMessages;

    @Getter
    MutableLiveData<Status> getFromStatus;

    @Getter
    MutableLiveData<Status> messagesStatus;

    @Getter
    MutableLiveData<Status> sendMessageStatus;

    @NonNull
    String fromEmail;

    public enum Status {
        ERROR,
        SUCCESS,
        NOT_FOUND
    }
    @Inject
    public MessageVM(MessageRepository messageRepository, @Named("fromEmail") String fromEmail) {
        this.messageRepository = messageRepository;
        this.getFromStatus  = new MutableLiveData<>();
        this.messagesStatus = new MutableLiveData<>();
        this.sendMessageStatus = new MutableLiveData<>();
        this.fromEmail = fromEmail;
        LiveData<List<Message>> fromMessagesLiveData = getFrom();
        fromMessages = new MediatorLiveData<>();
        fromMessages.addSource(fromMessagesLiveData, messages -> {
            if(!messages.isEmpty()) {
                fromMessages.removeSource(fromMessagesLiveData);
                fromMessages.setValue(messages);
            }
        });
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }

    private LiveData<List<Message>> getFrom() {
        return LiveDataReactiveStreams.fromPublisher(messageRepository.getMessageFrom(fromEmail).onErrorReturn(throwable -> {
            getFromStatus.postValue(MessageVM.Status.ERROR);
            return new ArrayList<>();
        }));
    }

    public LiveData<Message> sendMessage(String senderEmail,  String receiverEmail, String text) {
        AddMessageRequest request = new AddMessageRequest();
        request.setReceiverEmail(receiverEmail);
        request.setSenderEmail(senderEmail);
        request.setText(text);
        return LiveDataReactiveStreams.fromPublisher(messageRepository.addMessage(request).onErrorReturn(throwable -> {
            if(((HttpException) throwable).code() >= 300) {
                sendMessageStatus.postValue(MessageVM.Status.ERROR);
            }
            return new Message();
        }));
    }

    public LiveData<Message> getMessages() {
        return LiveDataReactiveStreams.fromPublisher(messageRepository.getMessages().onErrorReturn(throwable -> {
            messagesStatus.postValue(Status.ERROR);
            throwable.printStackTrace();
            return new Message();
        }));
    }
}
