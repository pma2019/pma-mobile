package org.pma.fitnessmatcher.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.LiveDataReactiveStreams;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import org.pma.fitnessmatcher.model.Document;
import org.pma.fitnessmatcher.model.TrainerProfile;
import org.pma.fitnessmatcher.repository.DocumentRepository;
import org.pma.fitnessmatcher.repository.UserRepository;

import java.io.File;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.disposables.CompositeDisposable;
import lombok.Getter;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class TrainerProfileVM extends ViewModel {
    private UserRepository userRepository;
    private DocumentRepository documentRepository;
    @Getter
    MutableLiveData<Status> addPictureStatus;
    @Getter
    MutableLiveData<Status> trainerLoadStatus;

    @Getter
    MediatorLiveData<TrainerProfile> trainer;

    MediatorLiveData<TrainerProfile> trainerProfile;

    CompositeDisposable compositeDisposable;

    private String trainerProfileEmail;

    public enum Status {
        ERROR,
        NOT_FOUND,
        SUCCESS
    }

    @Inject
    public TrainerProfileVM(UserRepository userRepository, DocumentRepository documentRepository, @Named("trainerProfileEmail") String trainerProfileEmail) {
        this.userRepository = userRepository;
        this.documentRepository = documentRepository;
        trainerLoadStatus = new MutableLiveData<>();
        addPictureStatus = new MutableLiveData<>();
        this.trainerProfileEmail = trainerProfileEmail;
        LiveData<TrainerProfile> trainerLiveData = trainer(this.trainerProfileEmail);
        this.trainer = new MediatorLiveData<>();
        this.trainer.addSource(trainerLiveData, t -> {
            if(t != null && t.getId() != null){
                this.trainer.removeSource(trainerLiveData);
                this.trainer.setValue(t);
            }
        });

    }

    public LiveData<TrainerProfile> trainer(String email){
        return LiveDataReactiveStreams.fromPublisher(userRepository.getTrainerProfile(email).onErrorReturn(throwable -> {
            getTrainerLoadStatus().postValue(Status.ERROR);
            return new TrainerProfile();
        }));
    }


    public LiveData<Document> addPicture(File imageFile) {
        MultipartBody.Part filePart = MultipartBody.Part.createFormData("image", imageFile.getName(), RequestBody.create(MediaType.parse("image/*"), imageFile));
        MultipartBody.Part emailPart = MultipartBody.Part.createFormData("email", documentRepository.getUserSessionManager().getUserDetails().getEmail());
        MultipartBody.Part avatarPart = MultipartBody.Part.createFormData("avatar", Boolean.toString(false));
        return LiveDataReactiveStreams.fromPublisher(documentRepository.addPicture(filePart, emailPart, avatarPart).onErrorReturn(throwable -> {
            addPictureStatus.postValue(Status.ERROR);
            return new Document();
        }));
    }



}
