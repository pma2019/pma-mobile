package org.pma.fitnessmatcher.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.LiveDataReactiveStreams;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import org.pma.fitnessmatcher.UserSessionManager;
import org.pma.fitnessmatcher.api.UserApi;
import org.pma.fitnessmatcher.api.requests.PasswordChange;
import org.pma.fitnessmatcher.model.UserProfile;
import org.pma.fitnessmatcher.repository.UserRepository;

import javax.inject.Inject;

import lombok.Getter;
import retrofit2.HttpException;

public class PrivacySettingsVM extends ViewModel {
    private UserRepository userRepository;

    @Getter
    private MutableLiveData<Status> changePasswordStatus;
    public enum Status {
        ERROR,
        SUCCESS,
        NOT_FOUND
    }

    @Inject
    public PrivacySettingsVM(UserRepository repository) {
        this.userRepository = repository;
        changePasswordStatus = new MutableLiveData<>();
    }

    public LiveData<UserProfile> changePassword(String email, String newPassword, String oldPassword){
        PasswordChange request = new PasswordChange();
        request.setEmail(email);
        request.setNewPassword(newPassword);
        request.setOldPassword(oldPassword);
        return LiveDataReactiveStreams.fromPublisher(userRepository.changePassword(request).onErrorReturn(e -> {
            changePasswordStatus.postValue(Status.ERROR);

            return new UserProfile();
        }));
    }

}
