package org.pma.fitnessmatcher.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.LiveDataReactiveStreams;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import org.pma.fitnessmatcher.InboxFragment;
import org.pma.fitnessmatcher.UserSessionManager;
import org.pma.fitnessmatcher.api.MessagesApi;
import org.pma.fitnessmatcher.model.Message;
import org.pma.fitnessmatcher.repository.MessageRepository;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import lombok.Getter;
import retrofit2.HttpException;

public class InboxVM extends ViewModel {
    MessageRepository messageRepository;
    @Getter
    MutableLiveData<Status> getInboxStatus;

    @Getter
    MediatorLiveData<List<Message>> inbox;

    public enum Status {
        ERROR,
        SUCCESS,
        NOT_FOUND
    }

    @Inject
    public InboxVM(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
        this.getInboxStatus = new MutableLiveData<>();
        LiveData<List<Message>> inboxLive = getInboxForUser();
        this.inbox = new MediatorLiveData<>();
        this.inbox.addSource(inboxLive, messages -> {
            if(!messages.isEmpty()) {
                this.inbox.removeSource(inboxLive);
                this.inbox.setValue(messages);
            }
        });
    }

    private LiveData<List<Message>> getInboxForUser() {
        return LiveDataReactiveStreams.fromPublisher(messageRepository.getInbox().onErrorReturn(throwable -> {
            getInboxStatus.postValue(Status.ERROR);
            return new ArrayList<>();
        }));

    }
}
