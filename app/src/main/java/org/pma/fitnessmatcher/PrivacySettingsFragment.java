package org.pma.fitnessmatcher;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.lifecycle.ViewModelProviders;

import org.pma.fitnessmatcher.viewmodel.PrivacySettingsVM;
import org.pma.fitnessmatcher.viewmodel.ViewModelProviderFactory;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;


public class PrivacySettingsFragment extends DaggerFragment {

    private PrivacySettingsVM viewModel;
    @Inject
    ViewModelProviderFactory providerFactory;

    @Inject
    protected UserSessionManager userSessionManager;

    public static PrivacySettingsFragment newInstance(){
        PrivacySettingsFragment privacyFragment = new PrivacySettingsFragment();
        return privacyFragment;
    }
    public PrivacySettingsFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile_privacy, container, false);
        final TextView password = rootView.findViewById(R.id.password);
        final TextView newPassword = rootView.findViewById(R.id.password2);
        Button saveBtn = rootView.findViewById(R.id.privacy_save_btn);

        viewModel = ViewModelProviders.of(this, providerFactory).get(PrivacySettingsVM.class);

        saveBtn.setOnClickListener(v -> {
            viewModel.changePassword(userSessionManager.getUserDetails().getEmail(), newPassword.getText().toString(), password.getText().toString())
                    .observe(getViewLifecycleOwner(), userProfile -> {
                        if(userProfile.getId() != null) {
                            Toast toast = Toast.makeText(getContext(), getResources().getString(R.string.password_change_success), getResources().getInteger(R.integer.duration));
                            toast.setGravity(Gravity.TOP|Gravity.RIGHT, getResources().getInteger(R.integer.toast_xoffset), getResources().getInteger(R.integer.toast_yoffset));
                            toast.show();
                        }
                    });
        });

        viewModel.getChangePasswordStatus().observe(getViewLifecycleOwner(), status -> {
            if(status.equals(PrivacySettingsVM.Status.ERROR)) {
                Toast toast = Toast.makeText(getContext(), getResources().getString(R.string.password_change_failed), getResources().getInteger(R.integer.duration));
                toast.setGravity(Gravity.TOP|Gravity.RIGHT, getResources().getInteger(R.integer.toast_xoffset), getResources().getInteger(R.integer.toast_yoffset));
                toast.show();
            }
        });
        return rootView;
    }
}