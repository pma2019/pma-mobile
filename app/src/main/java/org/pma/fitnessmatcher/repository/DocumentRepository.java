package org.pma.fitnessmatcher.repository;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Environment;

import org.pma.fitnessmatcher.UserSessionManager;
import org.pma.fitnessmatcher.api.DocumentApi;
import org.pma.fitnessmatcher.model.Document;

import java.io.File;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;

@Singleton
public class DocumentRepository extends Repository {
    private DocumentApi documentApi;

    @Inject
    public DocumentRepository(DocumentApi documentApi, UserSessionManager userSessionManager) {
        this.userSessionManager = userSessionManager;
        this.documentApi = documentApi;
    }

    public Flowable<Document> changeAvatar(MultipartBody.Part filePart,
                                           MultipartBody.Part email,
                                           MultipartBody.Part isAvatar) {
        return this.documentApi.changeAvatar(userSessionManager.getAuthHeader(), filePart, email, isAvatar).subscribeOn(Schedulers.io());
    }

    public Flowable<Document> addPicture(MultipartBody.Part filePart,
                                           MultipartBody.Part email,
                                           MultipartBody.Part isAvatar) {
        return this.documentApi.addPicture(userSessionManager.getAuthHeader(), filePart, email, isAvatar).subscribeOn(Schedulers.io());
    }



}
