package org.pma.fitnessmatcher.repository;

import org.pma.fitnessmatcher.UserSessionManager;

import java.util.Date;
import java.util.Map;

import lombok.Getter;

public abstract class Repository {
    @Getter
    protected UserSessionManager userSessionManager;
    protected static int FRESH_TIMEOUT_IN_MINUTES = 10;

    public boolean doRefresh(String key) {
        Date now = new Date();
        return userSessionManager.getLastRefresh(key) == 0 || userSessionManager.getLastRefresh(key) - now.getTime() > FRESH_TIMEOUT_IN_MINUTES * 60 * 1000;
    }

    public void updateRefresh(String key, Date date) {
        userSessionManager.saveLastRefresh(key, date.getTime());
    }

}
