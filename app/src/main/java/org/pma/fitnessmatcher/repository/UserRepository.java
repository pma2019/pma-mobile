package org.pma.fitnessmatcher.repository;

import androidx.lifecycle.LiveData;

import org.pma.fitnessmatcher.UserSessionManager;
import org.pma.fitnessmatcher.api.UserApi;
import org.pma.fitnessmatcher.api.requests.PasswordChange;
import org.pma.fitnessmatcher.database.dao.TrainerDao;
import org.pma.fitnessmatcher.model.TrainerProfile;
import org.pma.fitnessmatcher.model.UserProfile;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;

@Singleton
public class UserRepository extends Repository{
    private UserApi userApi;
    private TrainerDao trainerDao;

    @Inject
    public UserRepository(UserApi userApi, TrainerDao trainerDao, UserSessionManager userSessionManager) {
        this.userApi = userApi;
        this.trainerDao = trainerDao;
        this.userSessionManager = userSessionManager;
    }

    public Flowable<TrainerProfile> getTrainerProfile(String email) {
        return userApi.getTrainer(userSessionManager.getAuthHeader(), email).subscribeOn(Schedulers.io());
    }

    public Flowable<UserProfile> changePassword(PasswordChange request) {

        return userApi.changePassword(userSessionManager.getAuthHeader(), request).subscribeOn(Schedulers.io());
    }

    public Flowable<List<TrainerProfile>> getTrainers() {
        return userApi.getTrainers(userSessionManager.getAuthHeader()).subscribeOn(Schedulers.io());
    }

    public LiveData<TrainerProfile> getTrainerProfileDB(String email) {
        return trainerDao.getByEmail(email);
    }

    public LiveData<List<TrainerProfile>> getAllTrainerProfileDB() {
        return trainerDao.getAll();
    }


    public void insert(TrainerProfile trainer) {
        trainerDao.insert(trainer);
    }

    public void update(TrainerProfile trainer) {
        trainerDao.insert(trainer);
    }
}
