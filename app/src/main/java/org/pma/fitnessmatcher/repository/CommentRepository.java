package org.pma.fitnessmatcher.repository;

import org.pma.fitnessmatcher.UserSessionManager;
import org.pma.fitnessmatcher.api.CommentApi;
import org.pma.fitnessmatcher.api.requests.AddCommentRequest;
import org.pma.fitnessmatcher.model.Comment;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;

@Singleton
public class CommentRepository extends Repository {
    private CommentApi commentApi;

    @Inject
    public CommentRepository(CommentApi commentApi, UserSessionManager userSessionManager) {
        this.commentApi = commentApi;
        this.userSessionManager = userSessionManager;
    }

    public Flowable<List<Comment>> getAllByTimeSlot(Integer id) {
        return commentApi.getAllByTimeSlot(userSessionManager.getAuthHeader(), id).subscribeOn(Schedulers.io());
    }

    public Flowable<Comment> addComment(AddCommentRequest request) {
        return commentApi.addComment(userSessionManager.getAuthHeader(), request).subscribeOn(Schedulers.io());
    }
}
