package org.pma.fitnessmatcher.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.LiveDataReactiveStreams;

import org.pma.fitnessmatcher.UserSessionManager;
import org.pma.fitnessmatcher.api.TimeSlotApi;
import org.pma.fitnessmatcher.api.requests.MakeAppointmentRequest;
import org.pma.fitnessmatcher.model.TimeSlot;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;

@Singleton
public class TimeSlotRepository extends Repository {
    private TimeSlotApi timeSlotApi;

    private final String BY_TRAINEE = TimeSlotRepository.class.getSimpleName() + "BY_TRAINEE";
    private final String BY_TRAINER = TimeSlotRepository.class.getSimpleName() + "BY_TRAINER";

    @Inject
    public TimeSlotRepository(TimeSlotApi timeSlotApi, UserSessionManager userSessionManager) {

        this.timeSlotApi = timeSlotApi;
        this.userSessionManager = userSessionManager;
    }

    public Flowable<List<TimeSlot>> getAllByTrainee() {
        return timeSlotApi
                .getAllByTrainee(userSessionManager.getAuthHeader(), userSessionManager.getUserDetails().getEmail()).subscribeOn(Schedulers.io());
    }

    public Flowable<List<TimeSlot>> getAllByTrainer(String trainerEmail) {
        return timeSlotApi.getAllByTrainer(userSessionManager.getAuthHeader(), trainerEmail).subscribeOn(Schedulers.io());
    }

    public Flowable<TimeSlot> addTimeslotTrainee(MakeAppointmentRequest request) {
        return timeSlotApi.addTimeslotTrainee(userSessionManager.getAuthHeader(), request).subscribeOn(Schedulers.io());
    }




}
