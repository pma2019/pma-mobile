package org.pma.fitnessmatcher.repository;


import org.pma.fitnessmatcher.UserSessionManager;
import org.pma.fitnessmatcher.api.RatingApi;
import org.pma.fitnessmatcher.api.requests.AddRatingRequest;
import org.pma.fitnessmatcher.api.responses.OverAllRating;
import org.pma.fitnessmatcher.model.Ratings;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;

@Singleton
public class RatingsRepository extends Repository {
    private RatingApi ratingApi;
    @Inject
    public RatingsRepository(RatingApi ratingApi, UserSessionManager userSessionManager) {
        this.ratingApi = ratingApi;
        this.userSessionManager = userSessionManager;
    }
    public Flowable<Ratings> getByTraineeAndTimeSlot(Integer id) {
       return ratingApi.findByTraineeAndTimeSlot(userSessionManager.getAuthHeader(),
               userSessionManager.getUserDetails().getEmail(), id).subscribeOn(Schedulers.io());
    }

    public Flowable<OverAllRating> getByTrainerAndTimeSlot(Integer id) {
        return ratingApi.findByTrainerAndTimeSlot(userSessionManager.getAuthHeader(), userSessionManager.getUserDetails().getEmail(), id).subscribeOn(Schedulers.io());
    }

    public Flowable<Ratings> addRating(AddRatingRequest request) {
        return ratingApi.addRating(userSessionManager.getAuthHeader(), request).subscribeOn(Schedulers.io());
    }
}
