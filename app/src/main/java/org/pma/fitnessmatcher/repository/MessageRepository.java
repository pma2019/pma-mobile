package org.pma.fitnessmatcher.repository;

import com.google.gson.Gson;

import org.pma.fitnessmatcher.UserSessionManager;
import org.pma.fitnessmatcher.api.MessagesApi;
import org.pma.fitnessmatcher.api.requests.AddMessageRequest;
import org.pma.fitnessmatcher.model.Message;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;
import ua.naiksoftware.stomp.Stomp;
import ua.naiksoftware.stomp.StompClient;

@Singleton
public class MessageRepository extends Repository {
    MessagesApi messagesApi;
    Gson gson;
    StompClient stompClient;
    
    @Inject
    public MessageRepository(MessagesApi messagesApi, UserSessionManager userSessionManager, StompClient stompClient, Gson gson) {
        this.messagesApi = messagesApi;
        this.userSessionManager = userSessionManager;
        this.stompClient = stompClient;
        this.stompClient.connect();
        this.gson = gson;
    }

    public Flowable<List<Message>> getInbox() {
        return messagesApi.getInbox(userSessionManager.getAuthHeader()).subscribeOn(Schedulers.io());
    }

    public Flowable<List<Message>> getMessageFrom(String email) {
        return messagesApi.getMessagesFrom(userSessionManager.getAuthHeader(), email).subscribeOn(Schedulers.io());
    }

    public Flowable<Message> addMessage(AddMessageRequest request) {
        return messagesApi.addMessageFlow(userSessionManager.getAuthHeader(), request).subscribeOn(Schedulers.io());
    }

    public Flowable<Message> getMessages() {
        return stompClient.topic("/topic/reply")
                .map(stompMessage ->  gson.fromJson(stompMessage.getPayload(), Message.class))
                .filter(message -> message.getReceiverEmail().equals(getUserSessionManager().getUserDetails().getEmail()))
                .subscribeOn(Schedulers.io());
    }
}
