package org.pma.fitnessmatcher;

import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.pma.fitnessmatcher.adapter.TrainerChooserRecyclerViewAdapter;
import org.pma.fitnessmatcher.model.TrainerProfile;
import org.pma.fitnessmatcher.viewmodel.TrainerChooserVM;
import org.pma.fitnessmatcher.viewmodel.ViewModelProviderFactory;

import java.util.ArrayList;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import retrofit2.Retrofit;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnTrainerChooserInteractionListener}
 * interface.
 */
public class TrainerChooserFragment extends DaggerFragment {

    private static final String ARG_COLUMN_COUNT = "column-count";
    private int mColumnCount = 1;
    private OnTrainerChooserInteractionListener mListener;

    @Inject
    protected UserSessionManager userSessionManager;

    @Inject
    TrainerChooserVM viewModel;

    @Inject
    ViewModelProviderFactory providerFactory;

    TrainerChooserRecyclerViewAdapter adapter;

    @Inject
    protected Retrofit retrofit;
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public TrainerChooserFragment() {
    }


    public static TrainerChooserFragment newInstance(int columnCount) {
        TrainerChooserFragment fragment = new TrainerChooserFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.trainer_chooser_list, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.list);

        if (mColumnCount <= 1) {
            recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(recyclerView.getContext(), mColumnCount));
        }
        adapter = new TrainerChooserRecyclerViewAdapter(new ArrayList<TrainerProfile>(), mListener);
        recyclerView.setAdapter(adapter);

        viewModel = ViewModelProviders.of(this, providerFactory).get(TrainerChooserVM.class);
        subscribe();

        return view;
    }

    private void subscribe() {
        viewModel.getAllTrainers().observe(getViewLifecycleOwner(), trainerProfiles -> {
            if(!trainerProfiles.isEmpty()) {
                trainerProfiles.forEach(t -> adapter.addTrainer(t));
            }
        });
        viewModel.getStatus().observe(getViewLifecycleOwner(), status -> {
            if(status.equals(TrainerChooserVM.Status.ERROR)) {
                Toast toast = Toast.makeText(getContext(), getResources().getString(R.string.trainer_load_fail), getResources().getInteger(R.integer.duration));
                toast.setGravity(Gravity.TOP|Gravity.RIGHT, getResources().getInteger(R.integer.toast_xoffset), getResources().getInteger(R.integer.toast_yoffset));
                toast.show();
            }
        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnTrainerChooserInteractionListener) {
            mListener = (OnTrainerChooserInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnTrainerChooserInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in tonTrainerChooserItemTouchhis fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnTrainerChooserInteractionListener {
        void onTrainerChooserItemTouch(TrainerProfile item);
    }
}
