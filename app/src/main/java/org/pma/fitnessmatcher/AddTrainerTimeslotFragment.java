package org.pma.fitnessmatcher;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;

import org.pma.fitnessmatcher.adapter.TrainerTimeSlotsRecyclerViewAdapter;
import org.pma.fitnessmatcher.api.TimeSlotApi;
import org.pma.fitnessmatcher.api.requests.AddTimeslotTrainerRequest;
import org.pma.fitnessmatcher.model.TimeSlot;

import java.io.IOException;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

public class AddTrainerTimeslotFragment extends DaggerFragment {

    private static final String ARG_COLUMN_COUNT = "column-count";
    private int mColumnCount = 1;
    private AddTrainerTimeslotFragment.OnAddTrainerTimeSloInteractionListener tListener;

    private AddTrainerTimeslotsActivity.AddTrainerTimeslotTask task=null;

    private EditText mYearview;
    private EditText mMonthView;
    private EditText mDayView;
    private EditText mHourView;
    private Button mAddTs;

    @Inject
    protected Retrofit retrofit;

    @Inject
    UserSessionManager userSessionManager;

    public AddTrainerTimeslotFragment(){}

    public static AddTrainerTimeslotFragment newInstance(int columnCount){
        AddTrainerTimeslotFragment fragment=new AddTrainerTimeslotFragment();
        Bundle args=new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof AddTrainerTimeslotFragment.OnAddTrainerTimeSloInteractionListener){
            tListener=(AddTrainerTimeslotFragment.OnAddTrainerTimeSloInteractionListener) context;
        }else{
            throw new RuntimeException(context.toString()
            +" must implement OnAddTrainerTimeSloInteractionListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.activity_add_timeslot_trainer, container, false);
        LinearLayout linearLayout=view.findViewById(R.id.add_ts_form);

        mYearview=view.findViewById(R.id.addYear);
        mMonthView=view.findViewById(R.id.addMonth);
        mDayView=view.findViewById(R.id.addDay);
        mHourView=view.findViewById(R.id.addHour);
        mAddTs=(Button)view.findViewById(R.id.add_ts_trainer_button);

        mAddTs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptAdd();
            }
        });
        return view;
    }

    private void attemptAdd() {
        if (task != null) {
            return;
        }
        //   Integer year=mYearview.getText().;
        int year=Integer.parseInt(mYearview.getText().toString());
        int month=Integer.parseInt(mMonthView.getText().toString());
        int day=Integer.parseInt(mDayView.getText().toString());
        int hour=Integer.parseInt(mHourView.getText().toString());
        String email = userSessionManager.getUserDetails().getEmail();

        if(year!=0 && month!=0 && day!=0 && hour!=0 && !email.isEmpty() && email!=null){
            AddTrainerTimeslotFragment.AddTrainerTimeSlotTask task=new AddTrainerTimeslotFragment.AddTrainerTimeSlotTask((Integer) year,month,hour,day);
            task.execute((Void) null);
        }else{
            Toast toast=Toast.makeText(getContext(),"Morate popuniti sva polja.", Toast.LENGTH_LONG);
            toast.show();
        }
    }

    @RequiredArgsConstructor
    class AddTrainerTimeSlotTask extends AsyncTask<Void,Void,Boolean>{
        private @NonNull TrainerTimeSlotsRecyclerViewAdapter adapter;

        private final String mEmail;
        private final Integer mYear;
        private final Integer mMonth;
        private final Integer mDay;
        private final Integer mHour;

        AddTrainerTimeSlotTask(Integer mYear,Integer mMonth,Integer mDay,Integer mHour){
            mEmail= userSessionManager.getUserDetails().getEmail();
            this.mYear=mYear;
            this.mMonth=mMonth;
            this.mDay=mDay;
            this.mHour=mHour;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            TimeSlotApi api=retrofit.create(TimeSlotApi.class);
            AddTimeslotTrainerRequest request=new AddTimeslotTrainerRequest(mEmail,mYear,mMonth,mDay,mHour);
            Call<TimeSlot> call=api.addTimeslotTrainer(request);
            try{
                Response<TimeSlot> response=call.execute();
                if(response.isSuccessful()){
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.fragment_container, TrainerTimeSlotsFragment.newInstance(1));
                    transaction.addToBackStack(null);
                    transaction.commit();
                    return true;
                }else{
                    Toast toast = Toast.makeText(getContext(), "Doslo je do grekse.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP|Gravity.RIGHT, getResources().getInteger(R.integer.toast_xoffset), getResources().getInteger(R.integer.toast_yoffset));
                    toast.show();
                    return false;
                }
            }catch (IOException e){
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(final Boolean success) {}

    }

    @Override
    public void onDetach() {

        super.onDetach();
        tListener = null;
    }

    public interface OnAddTrainerTimeSloInteractionListener{
        void onAddTrainerTimeSlotInteraction(TimeSlot item);
    }
}
