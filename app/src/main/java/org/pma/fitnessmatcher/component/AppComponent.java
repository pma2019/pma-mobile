package org.pma.fitnessmatcher.component;

import org.pma.fitnessmatcher.MyApplication;
import org.pma.fitnessmatcher.UserSessionManager;
import org.pma.fitnessmatcher.module.builder.ActivityBuildersModule;
import org.pma.fitnessmatcher.module.AppModule;
import org.pma.fitnessmatcher.module.ViewModelFactoryModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(
        modules = {
                AndroidSupportInjectionModule.class,
                AppModule.class,
                ActivityBuildersModule.class,
                ViewModelFactoryModule.class
        }
)
public interface AppComponent extends AndroidInjector<MyApplication> {

    UserSessionManager userSessionManager();

    @Component.Builder
    interface Builder{

        @BindsInstance
        Builder application(MyApplication application);
        AppComponent build();
    }
}