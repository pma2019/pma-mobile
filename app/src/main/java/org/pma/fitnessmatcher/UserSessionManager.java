package org.pma.fitnessmatcher;

import android.content.Intent;
import android.content.SharedPreferences;

import org.pma.fitnessmatcher.api.UserApi;
import org.pma.fitnessmatcher.model.TrainerProfile;
import org.pma.fitnessmatcher.model.UserProfile;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import lombok.Getter;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

@Singleton
public class UserSessionManager {

    SharedPreferences preferences;

    SharedPreferences.Editor editor;


    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "FitnessMatcher";

    private static final String IS_LOGGED_IN = "IsLoggedIn";

    private static final String NAME = "name";

    private static final String EMAIL = "email";

    private static final String ID = "id";

    private static final String FIRST_NAME = "firstname";

    private static final String LAST_NAME = "lastname";

    private static final String REGISTRATION_DATE = "regdate";

    private static final String PICTURE = "profile_picture";

    private static final String TYPE = "dtype";

    private static final String TOKEN = "token";

    private Map<String, Long> lastRefresh;

    @Inject
    public UserSessionManager() {
        preferences = MyApplication.getContext().getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = preferences.edit();
        lastRefresh = new HashMap<>();
    }

    public void saveLastRefresh(String key, Long value) {
        editor.putLong(key, value);
        editor.commit();
    }

    public long getLastRefresh(String key) {
        return preferences.getLong(key, 0l);
    }

    public void createUserLoginSession(UserProfile profile, String token) {
        editor.putString(TOKEN, token);
        setUserProfileDetails(profile);
        editor.commit();
    }

    public boolean checkLogin() {
        if (!isLoggedIn()) {
            Intent intent = new Intent(MyApplication.getContext(), LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            MyApplication.getContext().startActivity(intent);

            return false;
        }

        return true;
    }
    public boolean isTrainer() {
        UserProfile user = getUserDetails();
        if(user != null) {
            return user.getDTYPE().equals(UserProfile.TRAINER_TYPE);
        }
        return false;
    }
    public boolean isTrainee() {
        UserProfile user = getUserDetails();
        if(user != null) {
            return user.getDTYPE().equals(UserProfile.TRAINEE_TYPE);
        }
        return false;
    }

    public boolean isLoggedIn() {
        return preferences.getBoolean(IS_LOGGED_IN, false);
    }

    public void logout() {
        editor.clear();
        editor.commit();
        checkLogin();
    }

    public String getToken() {
        return preferences.getString(TOKEN, "");
    }
    public String getAuthHeader() {
        return "Bearer " + preferences.getString(TOKEN, "");
    }


    public void setUserProfileDetails(UserProfile profile) {
        editor.putBoolean(IS_LOGGED_IN, true);
        editor.putInt(ID, profile.getId());
        editor.putString(NAME, profile.getUsername());
        editor.putString(EMAIL, profile.getEmail());

        editor.putString(FIRST_NAME, profile.getName());
        editor.putString(LAST_NAME, profile.getLastName());
        //editor.putString(REGISTRATION_DATE, profile.getRegistrationDate().t);
        editor.putString(PICTURE, profile.getProfilePictureStorageUrl());
        editor.putString(TYPE, profile.getDTYPE());
        editor.commit();
    }

    public UserProfile getUserDetails(){
        UserProfile profile = new UserProfile();
        if(!preferences.getBoolean(IS_LOGGED_IN, false)){
            return null;
        }
        profile.setId(preferences.getInt(ID, 0));
        profile.setUsername(preferences.getString(NAME, ""));
        profile.setEmail(preferences.getString(EMAIL, ""));
        profile.setName(preferences.getString(FIRST_NAME, ""));
        profile.setDTYPE(preferences.getString(TYPE, ""));
        profile.setLastName(preferences.getString(LAST_NAME, ""));
        profile.setProfilePictureStorageUrl(preferences.getString(PICTURE, ""));
        //editor.putString(REGISTRATION_DATE, profile.getRegistrationDate().t);
        return profile;
    }

}
