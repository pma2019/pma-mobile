package org.pma.fitnessmatcher.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import org.pma.fitnessmatcher.model.TimeSlot;

import java.util.List;

@Dao
public interface TimeSlotDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(TimeSlot timeSlot);

    @Update
    void update(TimeSlot timeSlot);

    @Delete
    void delete(TimeSlot timeSlot);

    @Query("DELETE FROM time_slot")
    void deleteAll();

    @Query("SELECT * FROM time_slot")
    LiveData<List<TimeSlot>> getAll();

    @Query("SELECT * FROM time_slot WHERE booked = 1")
    LiveData<List<TimeSlot>> getAllByTrainee();

    @Query("SELECT * FROM time_slot WHERE  trainerEmail like :trainerEmail")
    LiveData<List<TimeSlot>> getAllByTrainer(String trainerEmail);
}
