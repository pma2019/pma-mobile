package org.pma.fitnessmatcher.database;

import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import org.pma.fitnessmatcher.database.converter.DateConverter;
import org.pma.fitnessmatcher.database.dao.TrainerDao;
import org.pma.fitnessmatcher.model.TrainerProfile;


@androidx.room.Database(entities = {TrainerProfile.class}, version = 2, exportSchema = false)
@TypeConverters(DateConverter.class)
public abstract class Database extends RoomDatabase {

    public abstract TrainerDao getUserDao();

}

