package org.pma.fitnessmatcher.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import org.pma.fitnessmatcher.model.TrainerProfile;
import org.pma.fitnessmatcher.model.UserProfile;

import java.util.List;

import io.reactivex.Single;

@Dao
public interface TrainerDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(TrainerProfile trainerProfile);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(TrainerProfile trainerProfile);

    @Query("SELECT * FROM trainer_profile")
    LiveData<List<TrainerProfile>> getAll();

    @Query("SELECT * FROM trainer_profile WHERE email like :email")
    LiveData<TrainerProfile> getByEmail(String email);



}
