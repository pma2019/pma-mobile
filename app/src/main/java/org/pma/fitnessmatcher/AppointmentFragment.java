package org.pma.fitnessmatcher;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.pma.fitnessmatcher.adapter.AppointmentRecyclerViewAdapter;
import org.pma.fitnessmatcher.api.TimeSlotApi;
import org.pma.fitnessmatcher.api.requests.MakeAppointmentRequest;
import org.pma.fitnessmatcher.listeners.OnAppointmentItemInteractionListener;
import org.pma.fitnessmatcher.model.TimeSlot;
import org.pma.fitnessmatcher.viewmodel.AppointmentVM;
import org.pma.fitnessmatcher.viewmodel.TrainerTimeSlotsVM;
import org.pma.fitnessmatcher.viewmodel.ViewModelProviderFactory;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * A fragment representing a list of Items.
 * <p/>
 */
public class AppointmentFragment extends DaggerFragment implements OnAppointmentItemInteractionListener {

    public static final String ARG_COLUMN_COUNT = "column-count";
    public static final String ARG_TRAINER_EMAIL = "trainer-email";
    public static final String ARG_TRAINER_USERNAME = "trainer-username";
    private int mColumnCount = 1;
    private OnAppointmentItemInteractionListener mListener;

    @Inject
    protected UserSessionManager userSessionManager;

    AppointmentRecyclerViewAdapter adapter;

    @Inject
    ViewModelProviderFactory providerFactory;

    AppointmentVM viewModel;

    @Inject
    Retrofit retrofit;



    public AppointmentFragment() {
    }


    public static AppointmentFragment newInstance(int columnCount, String trainerEmail, String trainerUsername) {
        AppointmentFragment fragment = new AppointmentFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        args.putString(ARG_TRAINER_EMAIL, trainerEmail);
        args.putString(ARG_TRAINER_USERNAME, trainerUsername);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.appointment_list, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.appointment_list);
        Context context = recyclerView.getContext();
        if (mColumnCount <= 1) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
        }
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setTitle(getArguments().getString(ARG_TRAINER_USERNAME) + " appointments");

        adapter = new AppointmentRecyclerViewAdapter(new ArrayList<TimeSlot>(), mListener);
        recyclerView.setAdapter(adapter);

        viewModel = ViewModelProviders.of(this, providerFactory).get(AppointmentVM.class);
        subscribe();
        return view;
    }

    private void subscribe() {
        viewModel.getAllByTrainer().observe(getViewLifecycleOwner(), timeSlots -> timeSlots.forEach(t -> adapter.addAppointment(t)));

        viewModel.getMakeAppoinStatus().observe(getViewLifecycleOwner(), status -> {
            Toast toast = Toast.makeText(getContext(), getResources().getString(R.string.time_slots_load_trainer_fail), getResources().getInteger(R.integer.duration));
            toast.setGravity(Gravity.TOP|Gravity.RIGHT, getResources().getInteger(R.integer.toast_xoffset), getResources().getInteger(R.integer.toast_yoffset));
            toast.show();
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onListFragmentInteraction(TimeSlot item) {
        viewModel.makeAppointment(item.getId()).observe(getViewLifecycleOwner(), timeSlot -> {
            Toast toast = Toast.makeText(getContext(), getResources().getString(R.string.time_slots_appointment), getResources().getInteger(R.integer.duration));
            toast.setGravity(Gravity.TOP | Gravity.RIGHT, getResources().getInteger(R.integer.toast_xoffset), getResources().getInteger(R.integer.toast_yoffset));
            toast.show();
        });
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

}
