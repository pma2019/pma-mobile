package org.pma.fitnessmatcher.model;

import com.google.gson.annotations.Expose;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
// NECE SE KESIRATI U BAZU
public class Ratings {
    @Expose
    private Integer id;
    @Expose
    private Float ratingNumber;
    @Expose
    private String traineeEmail;
    @Expose
    private Integer timeSlotId;

}
