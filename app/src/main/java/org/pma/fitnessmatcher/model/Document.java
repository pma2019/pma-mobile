package org.pma.fitnessmatcher.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Document {
    @Expose
    private Integer id;
    @Expose
    private String name;
    @Expose
    private Date creationDate;
    @Expose
    private Date updateDate;
    @Expose
    private Boolean isActive;
    @Expose
    @SerializedName("storageUrl")
    private String imagePath;
}
