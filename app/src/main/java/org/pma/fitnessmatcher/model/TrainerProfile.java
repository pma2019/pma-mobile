package org.pma.fitnessmatcher.model;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.pma.fitnessmatcher.database.converter.DateConverter;

import java.sql.Date;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity(tableName = "trainer_profile")
public class TrainerProfile {

    @Expose
    @PrimaryKey(autoGenerate = false)
    private Integer id;
    @Expose
    private String username;
    @Expose
    private String password;
    @Expose
    private String email;
    @Expose
    private String name;
    @Expose
    private String lastName;
    @Expose
    private Date registrationDate;
    @Expose
    private Boolean isActive;
    @Expose
    private String profilePictureStorageUrl;

    @Expose
    private String gymName;
    @Expose
    private String bio;

    @Expose
    @SerializedName("pictures")
    @Ignore
    private List<Document> images;

    @Expose
    private Float overallRatings;
}
