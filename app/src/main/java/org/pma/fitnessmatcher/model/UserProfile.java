package org.pma.fitnessmatcher.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor @Getter @Setter
public class UserProfile {
    public static final String TRAINER_TYPE = "trainer";
    public static final String TRAINEE_TYPE = "trainee";
    @Expose
    private Integer id;
    @Expose
    private String username;
    @Expose
    private String password;
    @Expose
    private String email;
    @Expose
    private String name;
    @Expose
    private String lastName;
    @Expose
    private Date registrationDate;
    @Expose
    private Boolean isActive;
    @Expose
    private String profilePictureStorageUrl;

    @SerializedName("dtype")
    @Expose
    private String DTYPE;

}
