package org.pma.fitnessmatcher.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.sql.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @NoArgsConstructor
@Entity(tableName = "time_slot")
public class TimeSlot {
    @Expose
    @PrimaryKey
    private Integer id;
    @Expose
    private Integer year;
    @Expose
    private Integer month;
    @Expose
    private Integer day;
    @Expose
    private Integer hour;
    @Expose
    private String trainerName;
    @Expose
    private String trainerBio;
    @Expose
    private String trainerEmail;
    @Expose
    @SerializedName("trainerProfilePictureStorageUrl")
    private String trainerProfilePicture;
    @Expose
    private String trainerGymName;

    @Expose
    private Float overAllRatings;

    private Boolean booked;


}
