package org.pma.fitnessmatcher.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Setter
@Getter
@RequiredArgsConstructor
public class Message {
    @Expose
    private Integer id;

    @Expose
    private @NonNull String body;
    @Expose
    private @NonNull Date sentDate;

    @Expose
    private @NonNull Boolean isOpen;
    @Expose
    @SerializedName("senderProfilePictureStorageUrl")
    public @NonNull String senderProfilePicture;
    @Expose
    @SerializedName("receiverProfilePictureStorageUrl")
    public @NonNull String receiverProfilePicture;
    @Expose
    public String senderUsername;
    @Expose
    public String receiverUsername;
    @Expose
    public @NonNull String senderEmail;
    @Expose
    public @NonNull String receiverEmail;

}
