package org.pma.fitnessmatcher.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor @Getter @Setter
public class Comment {
    @Expose
    private Integer id;
    @Expose
    private Date creationDate;
    @Expose
    private Date updateDate;
    @Expose
    private Boolean isActive;
    @Expose
    private String body;

    @Expose
    private Integer timeSlotId;
    @Expose
    private String createByEmail;

    @Expose
    private String createByUsername;
    @Expose
    private Boolean byTrainer;

    @Expose
    @SerializedName("createByProfilePictureStorageUrl")
    private String profilePicture;

    @Expose
    private Comment reply;
}
