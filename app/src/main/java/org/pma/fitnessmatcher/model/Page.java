package org.pma.fitnessmatcher.model;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
public class Page<Entity> {
    private List<Entity> content;
    private Integer totalPages;
    private Integer totalElements;
    private Boolean last;
    private Boolean first;
    private Integer size;
    private Integer number;
    private Integer numberOfElements;

}
